package com.backend.footballkingdom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = "com.backend.footballkingdom.mapper")
@SpringBootApplication
@ComponentScan(basePackages = { "com.backend.footballkingdom" })
@EnableScheduling
public class FootballkingdomApplication {

    public static void main(String[] args) {
        SpringApplication.run(FootballkingdomApplication.class, args);
    }
}

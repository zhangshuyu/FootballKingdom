package com.backend.footballkingdom.config;

import com.backend.footballkingdom.security.ShiroService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.util.StringUtils;

import javax.servlet.Filter;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Configuration
public class ShiroConfigure {

    @Autowired
    private Realm realm;

    @Autowired
    private org.apache.shiro.mgt.SecurityManager securityManager;

    @Autowired
    private ShiroService shiroService;
    @Autowired
    AutowireCapableBeanFactory beanFactory;


    @Bean
    public ShiroFilterFactoryBean shiroFilter() {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(securityManager);
        bean.setLoginUrl("/signOut");
        Map<String, String> map = shiroService.getDefinitionMap();
        if (map == null) {
            map = new HashMap<String, String>();
        }
        //配置指定路径是否需要登录、或不需要登录,示例
        map.put("/login", "anon");
        map.put("/adminLogin", "anon");
        map.put("/test", "anon");
        map.put("/signOut", "anon");
        map.put("/verifyCode", "anon");
        map.put("/register", "anon");
        map.put("#/*", "anon");
        map.put("/fk/admin/*", "authc");//对该请求路径需要用户登录才能访问
        map.put("/fk/admin/**/*", "authc");//对该请求路径需要用户登录才能访问
        bean.setFilterChainDefinitionMap(map);
        Map<String, Filter> filterMap = new HashMap<String, Filter>();
        //替换默认的用户认证实现
        filterMap.put("authc", new FormAuthenticationFilter() {
            //重载跳转登录页面的逻辑，默认父类是使用重定向，这里改为返回标示，供前端判断
            @Override
            protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
                /*HttpServletRequest servletRequest = (HttpServletRequest) request;
                String type = servletRequest.getHeader("X-Requested-With");
                //对于未登录时发起的异步请求，直接返回约定的json字符串
                if ("XMLHttpRequest".equalsIgnoreCase(type)) {
                    response.setCharacterEncoding("UTF-8");
                    response.setContentType("application/json");
                    response.getWriter().write("{\"status\":\"401\",\"errorMsg\":\"尚未登录，请登录!\"}");
                } else {
                    super.redirectToLogin(request, response);
                }*/
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json");
                response.getWriter().write("{\"status\":\"401\",\"message\":\"尚未登录，请登录!\"}");
            }
        });
        bean.setFilters(filterMap);

        return bean;
    }

    @Bean
    public org.apache.shiro.mgt.SecurityManager securityManager() {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        DefaultWebSessionManager defaultWebSessionManager = new DefaultWebSessionManager();
        defaultWebSessionManager.getSessionIdCookie().setName("FOOTBALLKINGDOM_SESSION_ID");
        defaultWebSecurityManager.setSessionManager(defaultWebSessionManager);
        defaultWebSecurityManager.setRealm(realm);
        return defaultWebSecurityManager;
    }

    //修复session污染的bug
    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName("FOOTBALLKINGDOM_SESSION_ID");
        serializer.setCookiePath("/");
        serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
        return serializer;
    }

    @Bean
    public Realm realm() {
        AuthorizingRealm realm =
                new AuthorizingRealm() {
                    //获取当前用户的权限
                    @Override
                    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
                        if (principals == null) {
                            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
                        }

                        String username = (String) getAvailablePrincipal(principals);
                        // 查询用户的角色和权限
                        Set<String> roleNames = shiroService.getUserRoleNames(username);
                        Set<String> permissions = shiroService.getUserPermission(username);
                        // Retrieve roles and permissions from database
                        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
                        info.setStringPermissions(permissions);
                        return info;
                    }

                    //获取当前需要登录的用户信息，可以在这里校验用户名密码是否正确
                    @Override
                    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
                        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
                        String username = upToken.getUsername();
                        String password = String.valueOf(upToken.getPassword());

                        if (StringUtils.isEmpty(username)) {
                            throw new AuthenticationException("用户名不能为空！");
                        } else if (StringUtils.isEmpty(password)) {
                            throw new AuthenticationException("密码不能为空！");
                        } else if (!ShiroConfigure.this.shiroService.userExist(username, password)) {
                            throw new AuthenticationException("用户名或密码不正确！");
                        } else if (ShiroConfigure.this.shiroService.isPassed(username, password)) {
                            throw new AuthenticationException("账号已失效！请联系系统管理员！");
                        }
                        ShiroConfigure.this.shiroService.getUserRoleNames(username);

                        return new SimpleAuthenticationInfo(username, password.toCharArray(), getName());
                    }
                };
        return realm;
    }
}

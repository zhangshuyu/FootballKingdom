package com.backend.footballkingdom.config;

import com.alibaba.fastjson.JSONObject;
import com.backend.footballkingdom.enums.ZBBRequestMethod;
import com.backend.footballkingdom.util.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleTask {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 定时抓取直播吧的数据
     */
    //@Scheduled(fixedRate = 3600000)
    private void getData(){
        String GET_DATA_URL = ZBBRequestMethod.GET_DATA_URL.getUrl().replace("LEAGUE", "意甲").replace("TAB", "赛程");
        JSONObject httpResponse = HttpUtils.getHttpResponse(GET_DATA_URL,null);
        logger.info(httpResponse.get("data")+"");
    }
}

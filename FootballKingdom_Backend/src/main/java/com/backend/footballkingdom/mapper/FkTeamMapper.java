package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.team.FkTeam;
import tk.mybatis.mapper.common.Mapper;

public interface FkTeamMapper extends Mapper<FkTeam> {
}
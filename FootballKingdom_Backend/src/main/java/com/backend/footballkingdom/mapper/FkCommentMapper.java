package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.comment.FkComment;
import tk.mybatis.mapper.common.Mapper;

public interface FkCommentMapper extends Mapper<FkComment> {
}
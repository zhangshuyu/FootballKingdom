package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.news.FkNews;
import com.backend.footballkingdom.model.news.vo.NewsPageRequest;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkNewsMapper extends Mapper<FkNews> {

    List<FkNews> getAllNewsWithLeague(@Param("request") NewsPageRequest request);

    FkNews selectById(@Param("request") NewsPageRequest request);

}
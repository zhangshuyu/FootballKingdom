package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.league.FkLeague;
import tk.mybatis.mapper.common.Mapper;

public interface FkLeagueMapper extends Mapper<FkLeague> {
}
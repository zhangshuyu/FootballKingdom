package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.controller.player.vo.PlayerListRequest;
import com.backend.footballkingdom.model.playerList.FkPlayerList;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkPlayerListMapper extends Mapper<FkPlayerList> {

    List<FkPlayerList> findAll(@Param("request") PlayerListRequest request);

}
package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.user.FkMiniUser;
import tk.mybatis.mapper.common.Mapper;

public interface FkMiniUserMapper extends Mapper<FkMiniUser> {
}
package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.user.FkAdminUser;
import tk.mybatis.mapper.common.Mapper;

public interface FkAdminUserMapper extends Mapper<FkAdminUser> {
}
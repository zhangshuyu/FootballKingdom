package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.controller.teamList.vo.TeamListRequest;
import com.backend.footballkingdom.model.teamList.FkTeamList;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkTeamListMapper extends Mapper<FkTeamList> {

    List<FkTeamList> findAll(@Param("request") TeamListRequest request);

}
package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.ranking.FkRanking;
import com.backend.footballkingdom.model.ranking.vo.RankingPageRequest;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkRankingMapper extends Mapper<FkRanking> {
    List<FkRanking> selectRankingAllInfo( @Param("request") RankingPageRequest ranking);
}
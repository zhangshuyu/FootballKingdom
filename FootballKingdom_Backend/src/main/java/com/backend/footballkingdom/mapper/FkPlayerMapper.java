package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.player.FkPlayer;
import tk.mybatis.mapper.common.Mapper;

public interface FkPlayerMapper extends Mapper<FkPlayer> {
}
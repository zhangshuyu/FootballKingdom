package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.clubLeague.FkClubLeague;
import tk.mybatis.mapper.common.Mapper;

public interface FkClubLeagueMapper extends Mapper<FkClubLeague> {
}
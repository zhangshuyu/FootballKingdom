package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.games.FkGames;
import com.backend.footballkingdom.model.games.GamesPageRequest;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkGamesMapper extends Mapper<FkGames> {
    List<FkGames> findGamesWithLeagueAndTeam(GamesPageRequest gamesPageRequest);

    List<FkGames> findGamesByStartDayWithLeagueAndTeam(GamesPageRequest gamesPageRequest);
}
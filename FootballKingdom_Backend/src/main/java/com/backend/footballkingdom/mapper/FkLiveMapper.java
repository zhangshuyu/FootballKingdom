package com.backend.footballkingdom.mapper;

import com.backend.footballkingdom.model.live.FkLive;
import com.backend.footballkingdom.model.live.vo.LivePageRequest;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FkLiveMapper extends Mapper<FkLive> {
    List<FkLive> selectLiveAllInfo( @Param("request") LivePageRequest livePageRequest);
}
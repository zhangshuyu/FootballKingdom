package com.backend.footballkingdom.model.news.vo;

import lombok.Data;

@Data
public class NewsPageRequest {

    private String id;

    private int newsPageNum;

    private int newsPageSize;

    private String leagueId;

    private String newsDescription;

    private String newsType;

    private String importantWorldCupFlag;
    private String importantNewsFlag;
    private String recommendedNewsFlag;

    private String delFlag = "0";

}

package com.backend.footballkingdom.model.message;

import lombok.Data;

@Data
public class ImageMessage extends BaseMessage {
    // 图片链接
    private String PicUrl;
}

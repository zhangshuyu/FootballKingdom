package com.backend.footballkingdom.model.player;

import java.util.Date;
import javax.persistence.*;

/**
 * 球员表（暂时可不用到）
 */

@Table(name = "fk_player")
public class FkPlayer {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 球员名字
     */
    @Column(name = "player_name")
    private String playerName;

    /**
     * 描述
     */
    @Column(name = "player_description")
    private String playerDescription;

    /**
     * 头像
     */
    @Column(name = "player_head")
    private String playerHead;

    /**
     * 球员生日
     */
    @Column(name = "player_birthday")
    private Date playerBirthday;

    /**
     * 球员身价
     */
    @Column(name = "player_value")
    private String playerValue;

    /**
     * 球员人气
     */
    @Column(name = "player_popularity")
    private String playerPopularity;

    /**
     * 球员排名
     */
    @Column(name = "player_rank")
    private String playerRank;

    /**
     * 国家队ID
     */
    @Column(name = "country_id")
    private String countryId;

    /**
     * 俱乐部ID
     */
    @Column(name = "club_id")
    private String clubId;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取球员名字
     *
     * @return player_name - 球员名字
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * 设置球员名字
     *
     * @param playerName 球员名字
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * 获取描述
     *
     * @return player_description - 描述
     */
    public String getPlayerDescription() {
        return playerDescription;
    }

    /**
     * 设置描述
     *
     * @param playerDescription 描述
     */
    public void setPlayerDescription(String playerDescription) {
        this.playerDescription = playerDescription;
    }

    /**
     * 获取头像
     *
     * @return player_head - 头像
     */
    public String getPlayerHead() {
        return playerHead;
    }

    /**
     * 设置头像
     *
     * @param playerHead 头像
     */
    public void setPlayerHead(String playerHead) {
        this.playerHead = playerHead;
    }

    /**
     * 获取球员生日
     *
     * @return player_birthday - 球员生日
     */
    public Date getPlayerBirthday() {
        return playerBirthday;
    }

    /**
     * 设置球员生日
     *
     * @param playerBirthday 球员生日
     */
    public void setPlayerBirthday(Date playerBirthday) {
        this.playerBirthday = playerBirthday;
    }

    /**
     * 获取球员身价
     *
     * @return player_value - 球员身价
     */
    public String getPlayerValue() {
        return playerValue;
    }

    /**
     * 设置球员身价
     *
     * @param playerValue 球员身价
     */
    public void setPlayerValue(String playerValue) {
        this.playerValue = playerValue;
    }

    /**
     * 获取球员人气
     *
     * @return player_popularity - 球员人气
     */
    public String getPlayerPopularity() {
        return playerPopularity;
    }

    /**
     * 设置球员人气
     *
     * @param playerPopularity 球员人气
     */
    public void setPlayerPopularity(String playerPopularity) {
        this.playerPopularity = playerPopularity;
    }

    /**
     * 获取球员排名
     *
     * @return player_rank - 球员排名
     */
    public String getPlayerRank() {
        return playerRank;
    }

    /**
     * 设置球员排名
     *
     * @param playerRank 球员排名
     */
    public void setPlayerRank(String playerRank) {
        this.playerRank = playerRank;
    }

    /**
     * 获取国家队ID
     *
     * @return country_id - 国家队ID
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * 设置国家队ID
     *
     * @param countryId 国家队ID
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     * 获取俱乐部ID
     *
     * @return club_id - 俱乐部ID
     */
    public String getClubId() {
        return clubId;
    }

    /**
     * 设置俱乐部ID
     *
     * @param clubId 俱乐部ID
     */
    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    /**
     * 获取创建者
     *
     * @return create_by - 创建者
     */
    public Integer getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建者
     *
     * @param createBy 创建者
     */
    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新者
     *
     * @return update_by - 更新者
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新者
     *
     * @param updateBy 更新者
     */
    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取更新时间
     *
     * @return update_date - 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间
     *
     * @param updateDate 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取备注信息
     *
     * @return remarks - 备注信息
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 设置备注信息
     *
     * @param remarks 备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取删除标记
     *
     * @return del_flag - 删除标记
     */
    public String getDelFlag() {
        return delFlag;
    }

    /**
     * 设置删除标记
     *
     * @param delFlag 删除标记
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}
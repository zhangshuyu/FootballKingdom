package com.backend.footballkingdom.model.user;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "fk_mini_user")
public class FkMiniUser {
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    private String openid;

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "head_img")
    private String headImg;

    private String gender;

    private String country;

    private String city;

    private String province;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    public FkMiniUser() {
    }

    public FkMiniUser(String openid) {
        this.openid = openid;
    }
}
package com.backend.footballkingdom.model.comment;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "fk_comment")
public class FkComment {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    @Column(name = "live_id")
    private String liveId;

    private String content;

    @Column(name = "sender_id")
    private String senderId;

    @Column(name = "sender_name")
    private String senderName;

    @Column(name = "sender_head")
    private String senderHead;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

}
package com.backend.footballkingdom.model.ranking;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.team.FkTeam;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

/**
 * 积分榜表
 */

@Table(name = "fk_ranking")
@Data
public class FkRanking {
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 积分榜时间
     */
    @Column(name = "ranking_time")
    private String rankingTime;

    /**
     * 球队id
     */
    @Column(name = "team_id")
    private String teamId;

    /**
     * 场次
     */
    private String session;

    /**
     * 比赛种类（小组赛，第几轮）
     */
    private String kind;

    /**
     * 胜利场次
     */
    @Column(name = "win_times")
    private Integer winTimes;

    /**
     * 失败场次
     */
    @Column(name = "fail_times")
    private Integer failTimes;

    /**
     * 打平场数
     */
    @Column(name = "tie_times")
    private Integer tieTimes;

    /**
     * 进/失球
     */
    @Column(name = "score_or_lost")
    private String scoreOrLost;

    /**
     * 积分
     */
    private Integer ranking;

    /**
     * 联赛id
     */
    @Column(name = "league_id")
    private String leagueId;

    @Transient
    private FkTeam team;

    @Transient
    private FkLeague league;
}
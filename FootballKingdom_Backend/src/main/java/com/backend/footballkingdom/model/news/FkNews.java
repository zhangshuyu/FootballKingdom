package com.backend.footballkingdom.model.news;

import com.backend.footballkingdom.model.league.FkLeague;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

/**
 * 新闻表
 */
@Data
@Table(name = "fk_news")
public class FkNews {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 标题
     */
    @Column(name = "news_title")
    private String newsTitle;

    /**
     * 小标题
     */
    @Column(name = "news_title2")
    private String newsTitle2;

    /**
     * 大图
     */
    @Column(name = "news_big_pic")
    private String newsBigPic;

    /**
     * 联赛ID
     */
    @Column(name = "league_id")
    private String leagueId;

    /**
     * 推荐新闻标记
     */
    @Column(name = "recommended_news_flag")
    private String recommendedNewsFlag;

    /**
     * 链接
     */
    @Column(name = "news_url")
    private String newsUrl;

    /**
     * 重要新闻标记
     */
    @Column(name = "important_news_flag")
    private String importantNewsFlag;

    /**
     * 世界杯重要新闻标记
     */
    @Column(name = "important_world_cup_flag")
    private String importantWorldCupFlag;

    /**
     * 内容
     */
    @Column(name = "news_content")
    private String newsContent;

    /**
     * 图片1
     */
    @Column(name = "news_pic1")
    private String newsPic1;

    /**
     * 图片2
     */
    @Column(name = "news_pic2")
    private String newsPic2;

    /**
     * 图片3
     */
    @Column(name = "news_pic3")
    private String newsPic3;

    /**
     * 图片4
     */
    @Column(name = "news_pic4")
    private String newsPic4;

    /**
     * 图片5
     */
    @Column(name = "news_pic5")
    private String newsPic5;

    /**
     * 描述
     */
    @Column(name = "news_description")
    private String newsDescription;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private String createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private String updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    @Transient
    private FkLeague league;


}
package com.backend.footballkingdom.model.live.vo;

import lombok.Data;

@Data
public class LivePageRequest {
     private int livePageNum;
     private int livePageSize;
     private String id;
     private String delFlag;
     private String liveDate;
     private String leagueId;
     private String liveName;
     private String importantLiveFlag;
     private String importantWorldCupFlag;
     private String recommendedLiveFlag;
}

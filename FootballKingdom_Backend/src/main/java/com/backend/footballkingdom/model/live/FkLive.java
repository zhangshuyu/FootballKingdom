package com.backend.footballkingdom.model.live;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.team.FkTeam;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 直播表
 */

@Data
@Table(name = "fk_live")
public class FkLive {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 直播赛事名称
     */
    @Column(name = "live_name")
    private String liveName;

    /**
     * 直播二级标题（赛事视频来源）
     */
    @Column(name = "live_second_name")
    private String liveSecondName;

    /**
     * 直播大图
     */
    @Column(name = "live_big_pic")
    private String liveBigPic;


    /**
     * 直播赛事时间
     */
    @Column(name = "live_date")
    private String liveDate;

    /**
     * 直播赛事时间
     */
    @Column(name = "live_time")
    private String liveTime;

    /**
     * 主队
     */
    @Column(name = "live_team_main_id")
    private String liveTeamMainId;

    /**
     * 客队
     */
    @Column(name = "live_team_visiter_id")
    private String liveTeamVisiterId;

    /**
     * 链接
     */
    @Column(name = "live_url")
    private String liveUrl;

    /**
     * 联赛ID
     */
    @Column(name = "league_id")
    private String leagueId;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    private String ad;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    /**
     * 重要直播标记
     */
    @Column(name = "important_live_flag")
    private String importantLiveFlag;

    /**
     * 世界杯重要直播标记
     */
    @Column(name = "important_world_cup_flag")
    private String importantWorldCupFlag;

    /**
     * 推荐直播标记
     */
    @Column(name = "recommended_live_flag")
    private String recommendedLiveFlag;

    @Column(name = "live_source")
    private String liveSource;

    @Column(name = "live_status")
    private String liveStatus;

    @Transient
    private FkLeague league;

    @Transient
    private FkTeam mainTeam;

    @Transient
    private FkTeam visiterTeam;

    @Transient
    private List<Map<String,String>> lives;

}

package com.backend.footballkingdom.model.playerList;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.team.FkTeam;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

/**
 * 球员榜表
 */

@Data
@Table(name = "fk_player_list")
public class FkPlayerList {
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 球队id
     */
    @Column(name = "team_id")
    private String teamId;

    /**
     * 联赛id
     */
    @Column(name = "league_id")
    private String leagueId;


    /**
     * 球员榜时间
     */
    @Column(name = "play_list_time")
    private String playListTime;

    /**
     * 球员姓名
     */
    @Column(name = "player_name")
    private String playerName;

    /**
     * 球员头像
     */
    @Column(name = "player_head")
    private String playerHead;

    /**
     * 射手点球数
     */
    @Column(name = "shooter_penalty_total")
    private Integer shooterPenaltyTotal;

    /**
     * 助攻总数
     */
    @Column(name = "assists_total")
    private Integer assistsTotal;

    /**
     * 伤停原因
     */
    @Column(name = "injury_reason")
    private String injuryReason;

    /**
     * 伤停出场
     */
    @Column(name = "injury_appear_total")
    private Integer injuryAppearTotal;

    /**
     * 射门总数
     */
    @Column(name = "shoot_total")
    private Integer shootTotal;

    /**
     * 射中总数
     */
    @Column(name = "shooting_total")
    private Integer shootingTotal;

    /**
     * 传球总数
     */
    @Column(name = "pass_total")
    private Integer passTotal;

    /**
     * 传球成功率
     */
    @Column(name = "passing_success_rate")
    private Float passingSuccessRate;

    /**
     * 抢断总数
     */
    @Column(name = "steal_total")
    private Integer stealTotal;

    /**
     * 解围总数
     */
    @Column(name = "rescue_total")
    private Integer rescueTotal;

    /**
     * 扑救总数
     */
    @Column(name = "save_total")
    private Integer saveTotal;

    /**
     * 黄牌总数
     */
    @Column(name = "yellow_card_total")
    private Integer yellowCardTotal;

    /**
     * 红牌总数
     */
    @Column(name = "red_card_total")
    private Integer redCardTotal;

    /**
     * 出场次数
     */
    @Column(name = "apear_total")
    private Integer apearTotal;

    /**
     * 出场时间
     */
    @Column(name = "apear_time_total")
    private Integer apearTimeTotal;

    /**
     * 替换上场
     */
    @Column(name = "replace_before_total")
    private Integer replaceBeforeTotal;

    /**
     * 替换下场
     */
    @Column(name = "replace_after_total")
    private Integer replaceAfterTotal;

    /**
     * 击中门框总数
     */
    @Column(name = "hitdoor_total")
    private Integer hitdoorTotal;

    /**
     * 头球进球总数
     */
    @Column(name = "header_goal_total")
    private Integer headerGoalTotal;

    /**
     * 点球进球总数
     */
    @Column(name = "penalty_goal_total")
    private Integer penaltyGoalTotal;

    /**
     * 任意球进球总数
     */
    @Column(name = "free_kick_total")
    private Integer freeKickTotal;

    /**
     * 越位总数
     */
    @Column(name = "offside_total")
    private Integer offsideTotal;

    /**
     * 触球次数
     */
    @Column(name = "toubch_ball_total")
    private Integer toubchBallTotal;

    /**
     * 直塞总数
     */
    @Column(name = "strait_plug_total")
    private Integer straitPlugTotal;

    /**
     * 传中总数
     */
    @Column(name = "pass_in_total")
    private Integer passInTotal;

    /**
     * 角球总数
     */
    @Column(name = "corner_kick_total")
    private Integer cornerKickTotal;

    /**
     * 封堵总数
     */
    @Column(name = "plugging_total")
    private Integer pluggingTotal;

    /**
     * 拦截总数
     */
    @Column(name = "interception_total")
    private Integer interceptionTotal;

    /**
     * 头球解围总数
     */
    @Column(name = "header_clearance_total")
    private Integer headerClearanceTotal;

    /**
     * 争顶成功总数
     */
    @Column(name = "striving_success_total")
    private Integer strivingSuccessTotal;

    /**
     * 乌龙球
     */
    @Column(name = "own_goals_total")
    private Integer ownGoalsTotal;

    /**
     * 失误丢球总数
     */
    @Column(name = "miss_ball_total")
    private Integer missBallTotal;

    /**
     * 被罚点球总数
     */
    @Column(name = "penalized_penalty_total")
    private Integer penalizedPenaltyTotal;

    /**
     * 犯规次数
     */
    @Column(name = "foul_total")
    private Integer foulTotal;

    /**
     * 零分场次数
     */
    @Column(name = "zero_closures_total")
    private Integer zeroClosuresTotal;

    /**
     * 失球数
     */
    @Column(name = "conceded_total")
    private Integer concededTotal;

    /**
     * 扑出点球数
     */
    @Column(name = "save_peanalty_total")
    private Integer savePeanaltyTotal;

    /**
     * 手击球数
     */
    @Column(name = "hand_hitting_total")
    private Integer handHittingTotal;

    @Transient
    private FkLeague league;

    @Transient
    private FkTeam fkteam;
}
package com.backend.footballkingdom.model.ranking.vo;

import lombok.Data;

@Data
public class RankingPageRequest {
     private int rankingPageNum;
     private int rankingPageSize;
     private String rankingTime;
     private String leagueId;
     private String kind;
}

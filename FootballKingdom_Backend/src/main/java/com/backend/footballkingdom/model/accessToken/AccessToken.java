package com.backend.footballkingdom.model.accessToken;

import lombok.Data;

@Data
public class AccessToken {
    private String token;
    private int expiresIn;
}

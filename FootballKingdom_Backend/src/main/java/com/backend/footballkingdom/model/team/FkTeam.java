package com.backend.footballkingdom.model.team;

import java.util.Date;
import javax.persistence.*;

/**
 * 球队表
 */
@Table(name = "fk_team")
public class FkTeam {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 球队名称
     */
    @Column(name = "team_name")
    private String teamName;

    /**
     * 描述
     */
    @Column(name = "team_description")
    private String teamDescription;

    /**
     * 球队图标
     */
    @Column(name = "team_icon")
    private String teamIcon;

    /**
     * 球队种类
     */
    @Column(name = "team_kind")
    private String teamKind;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取球队名称
     *
     * @return team_name -球队名称
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * 设置球队名称
     *
     * @param teamName 球队名称
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * 获取描述
     *
     * @return team_description - 描述
     */
    public String getTeamDescription() {
        return teamDescription;
    }

    /**
     * 设置描述
     *
     * @param teamDescription 描述
     */
    public void setTeamDescription(String teamDescription) {
        this.teamDescription = teamDescription;
    }

    /**
     * 获取球队图标
     *
     * @return team_icon - 球队图标
     */
    public String getTeamIcon() {
        return teamIcon;
    }

    /**
     * 设置球队图标
     *
     * @param teamIcon 球队图标
     */
    public void setTeamIcon(String teamIcon) {
        this.teamIcon = teamIcon;
    }

    /**
     * 获取球队种类
     *
     * @return team_kind - 球队种类
     */
    public String getTeamKind() {
        return teamKind;
    }

    /**
     * 设置球队种类
     *
     * @param teamKind 球队种类
     */
    public void setTeamKind(String teamKind) {
        this.teamKind = teamKind;
    }

    /**
     * 获取创建者
     *
     * @return create_by - 创建者
     */
    public Integer getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建者
     *
     * @param createBy 创建者
     */
    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新者
     *
     * @return update_by - 更新者
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新者
     *
     * @param updateBy 更新者
     */
    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取更新时间
     *
     * @return update_date - 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间
     *
     * @param updateDate 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 获取备注信息
     *
     * @return remarks - 备注信息
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 设置备注信息
     *
     * @param remarks 备注信息
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取删除标记
     *
     * @return del_flag - 删除标记
     */
    public String getDelFlag() {
        return delFlag;
    }

    /**
     * 设置删除标记
     *
     * @param delFlag 删除标记
     */
    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }
}
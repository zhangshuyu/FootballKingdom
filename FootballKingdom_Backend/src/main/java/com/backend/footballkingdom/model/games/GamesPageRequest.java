package com.backend.footballkingdom.model.games;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Auther: Justyn
 * @Date: 2018/6/6 19:05
 */
@Data
public class GamesPageRequest {

    private int pageNum;

    private int pageSize;

    private String id;

    private String sgameName;

    private String sleagueId;

    private String sgameKind;

    private String  gameStartTime;
}

package com.backend.footballkingdom.model.teamList;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.team.FkTeam;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

/**
 * 球队榜表
 */

@Data
@Table(name = "fk_team_list")
public class FkTeamList {
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 球队id
     */
    @Column(name = "team_id")
    private String teamId;

    /**
     * 联赛id
     */
    @Column(name = "league_id")
    private String leagueId;

    /**
     * 球队榜时间
     */
    @Column(name = "team_list_time")
    private String teamListTime;

    /**
     * 进球总数
     */
    @Column(name = "goal_total")
    private Integer goalTotal;

    /**
     * 胜利总数
     */
    @Column(name = "win_total")
    private Integer winTotal;

    /**
     * 失败总数
     */
    @Column(name = "fail_total")
    private Integer failTotal;

    /**
     * 点球总数
     */
    @Column(name = "panalty_total")
    private Integer panaltyTotal;

    /**
     * 射门总数
     */
    @Column(name = "shoot_total")
    private Integer shootTotal;

    /**
     * 射中总数
     */
    @Column(name = "shooting_total")
    private Integer shootingTotal;

    /**
     * 传球总数
     */
    @Column(name = "pass_total")
    private Integer passTotal;

    /**
     * 传球成功率
     */
    @Column(name = "passing_success_rate")
    private Float passingSuccessRate;

    /**
     * 抢断总数
     */
    @Column(name = "steal_total")
    private Integer stealTotal;

    /**
     * 解围总数
     */
    @Column(name = "rescue_total")
    private Integer rescueTotal;

    /**
     * 扑救总数
     */
    @Column(name = "save_total")
    private Integer saveTotal;

    /**
     * 犯规场均
     */
    @Column(name = "foul_average")
    private Integer foulAverage;

    /**
     * 击中门框总数
     */
    @Column(name = "hitdoor_total")
    private Integer hitdoorTotal;

    /**
     * 头球得分总数
     */
    @Column(name = "header_total")
    private Integer headerTotal;

    /**
     * 任意球得分总数
     */
    @Column(name = "free_kick_total")
    private Integer freeKickTotal;

    /**
     * 禁区进球总数
     */
    @Column(name = "restricted_area_total")
    private Integer restrictedAreaTotal;

    /**
     * 禁区外进球总数
     */
    @Column(name = "outside_area_total")
    private Integer outsideAreaTotal;

    /**
     * 防守反击得分总数
     */
    @Column(name = "defensive_countattack_total")
    private Integer defensiveCountattackTotal;

    /**
     * 越位总数
     */
    @Column(name = "offside_total")
    private Integer offsideTotal;

    /**
     * 零分场次总数
     */
    @Column(name = "zero_closusers_total")
    private Integer zeroClosusersTotal;

    /**
     * 总失球数
     */
    @Column(name = "conceded_total")
    private Integer concededTotal;

    /**
     * 封堵总数
     */
    @Column(name = "plugging_total")
    private Integer pluggingTotal;

    /**
     * 拦截总数
     */
    @Column(name = "interception_total")
    private Integer interceptionTotal;

    /**
     * 头球解围总数
     */
    @Column(name = "header_clearance_total")
    private Integer headerClearanceTotal;

    /**
     * 乌龙球总数
     */
    @Column(name = "own_goals_total")
    private Integer ownGoalsTotal;

    /**
     * 被罚点球总数
     */
    @Column(name = "penalized_penalty_total")
    private Integer penalizedPenaltyTotal;

    /**
     * 点球被罚进总数
     */
    @Column(name = "penalty_kicked_in_total")
    private Integer penaltyKickedInTotal;

    /**
     * 直塞总数
     */
    @Column(name = "straight_plug_total")
    private Integer straightPlugTotal;

    /**
     * 长传总数
     */
    @Column(name = "long_pass_total")
    private Integer longPassTotal;

    /**
     * 回传球总数
     */
    @Column(name = "return_pass_total")
    private Integer returnPassTotal;

    /**
     * 传中总数
     */
    @Column(name = "pass_in_total")
    private Integer passInTotal;

    /**
     * 角球总数
     */
    @Column(name = "corner_kick_total")
    private Integer cornerKickTotal;

    @Transient
    private FkLeague league;

    @Transient
    private FkTeam fkteam;
}
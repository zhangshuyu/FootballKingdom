package com.backend.footballkingdom.model.games;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.team.FkTeam;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;


/**
 * 赛程表
 */
@Data
@Table(name = "fk_games")
public class FkGames {
    /**
     * ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 比赛名称
     */
    @Column(name = "game_name")
    private String gameName;

    /**
     * 描述
     */
    @Column(name = "game_description")
    private String gameDescription;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @Column(name = "game_datetime")
    private Date gameDatetime;

    @Column(name = "game_date")
    private String gameDate;

    @Column(name = "game_time")
    private String gameTime;

    /**
     * 联赛ID
     */
    @Column(name = "league_id")
    private String leagueId;

    /**
     * 主队
     */
    @Column(name = "game_team_main_id")
    private String gameTeamMainId;

    /**
     * 客队
     */
    @Column(name = "game_team_visiter_id")
    private String gameTeamVisiterId;

    /**
     * 比分
     */
    @Column(name = "game_score")
    private String gameScore;

    /**
     * 比赛种类（小组赛，第几轮）
     */
    @Column(name = "game_kind")
    private String gameKind;

    /**
     * 场次
     */
    @Column(name = "game_session")
    private String gameSession;

    /**
     * 创建者
     */
    @Column(name = "create_by")
    private Integer createBy;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column(name = "update_by")
    private Integer updateBy;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    @Column(name = "del_flag")
    private String delFlag;

    @Transient
    private FkLeague league;
    @Transient
    private FkTeam mainTeam;
    @Transient
    private FkTeam visiterTeam;


}
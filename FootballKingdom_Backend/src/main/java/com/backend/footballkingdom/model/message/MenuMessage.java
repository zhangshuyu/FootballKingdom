package com.backend.footballkingdom.model.message;

import lombok.Data;

@Data
public class MenuMessage extends BaseMessage{
    private String EventKey;
}

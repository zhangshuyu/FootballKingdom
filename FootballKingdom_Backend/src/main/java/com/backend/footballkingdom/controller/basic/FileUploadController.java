package com.backend.footballkingdom.controller.basic;

import com.aliyun.oss.OSSClient;
import com.backend.footballkingdom.model.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

@RestController
@RequestMapping(value = "/fk/admin/")
public class FileUploadController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value(value = "${OSSEndpoint}")
    private String OSSEndpoint;

    @Value(value = "${OSSAccessKeyId}")
    private String OSSAccessKeyId;

    @Value(value = "${OSSAccessKeySecret}")
    private String OSSAccessKeySecret;


    @Value(value = "${OSSBucket}")
    private String OSSBucket;

    @RequestMapping(value = "uploadIcon")
    @ResponseBody
    public Response uploadIcon(@RequestParam("file") MultipartFile file) {
        String filename=System.currentTimeMillis()+".png";
        logger.info("文件名自动生成———————>"+filename);
        logger.info("开始上传——————————>");
        URL url=null;
        if (!file.isEmpty()) {
            try {
                InputStream inputStream = file.getInputStream();
                OSSClient ossClient = new OSSClient(OSSEndpoint, OSSAccessKeyId, OSSAccessKeySecret);
                ossClient.putObject(OSSBucket, filename, inputStream);
                Date expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
                url = ossClient.generatePresignedUrl(OSSBucket, filename, expiration);
                logger.info("上传完成——————————>");
                logger.info("返回链接——————————>"+url);
                inputStream.close();
                ossClient.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
                return  new Response<>("上传发生错误，错误信息："+e.getMessage());
            }
        }
        return new Response<>(url);
    }
}

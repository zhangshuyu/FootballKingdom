package com.backend.footballkingdom.controller.user;

import com.backend.footballkingdom.controller.user.vo.MiniUserRequest;
import com.backend.footballkingdom.model.comment.FkComment;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.model.user.FkMiniUser;
import com.backend.footballkingdom.service.comment.FkCommentService;
import com.backend.footballkingdom.service.user.FkMiniUserService;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/fk/admin/user")
public class UserAdminController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkMiniUserService miniUserService;

    @Autowired
    private FkCommentService commentService;

    @GetMapping(value = "getTodayMiniUser")
    public Response getTodayMiniUser() {
        Response<List<FkMiniUser>> response = new Response<>();
        MiniUserRequest miniUser = new MiniUserRequest();
        miniUser.setCreateDate(DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATE_DEFAULT));
        response.setBody(miniUserService.getMiniUserByExample(miniUser));
        return response;
    }

    @PostMapping(value = "changeUserStatus")
    public Response changeUserStatus(@RequestBody FkMiniUser fkMiniUser) {
        Response response = new Response<>();
        Assert.isTrue(miniUserService.updateMiniUser(fkMiniUser) == 1, "改变状态失败");
        return response;
    }

    @GetMapping(value = "getYesterdayMiniUser")
    public Response getYesterdayMiniUser() {
        Response response = new Response<>();
        MiniUserRequest miniUser = new MiniUserRequest();
        Date s=DateUtil.getNextDay(new Date(), -1);
        miniUser.setCreateDate(DateUtil.formatDate(s, DateUtil.FORMAT_DATE_DEFAULT));
        response.setBody(miniUserService.getMiniUserByExample(miniUser));
        return response;
    }

    @GetMapping(value = "getAllMiniUsers")
    public Response getAllMiniUsers() {
        Response<List<FkMiniUser>> response = new Response<>();
        MiniUserRequest miniUser = new MiniUserRequest();
        response.setBody(miniUserService.getMiniUserByExample(miniUser));
        return response;
    }


    @PostMapping(value = "getMiniUserByPage")
    public Response getMiniUserByPage(@RequestBody MiniUserRequest miniUser) {
        Response response = new Response<>();
        Page<FkMiniUser> users=miniUserService.getMiniUserByPage(miniUser);
        response.setBody(users);
        response.setPageTotalNum(users.getTotal());
        return response;
    }
}

package com.backend.footballkingdom.controller.teamList;

import com.backend.footballkingdom.controller.teamList.vo.TeamListRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.model.teamList.FkTeamList;
import com.backend.footballkingdom.service.teamList.FkTeamListService;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/fk/admin/teamList")
public class TeamListAdminController {

    @Autowired
    private FkTeamListService teamListService;

    private static final String MSG_ADD_ERROR = "添加失败";
    private static final String MSG_UPDATE_ERROR = "更新失败";
    private static final String MSG_DELETE_ERROR = "删除失败";

    @PostMapping(value = "getByPage")
    public Response getByPage(@RequestBody TeamListRequest teamList) {
        Response<Page<FkTeamList>> response = new Response<>();
        Page<FkTeamList> listPage = teamListService.findByPage(teamList);
        response.setBody(listPage);
        response.setPageTotalNum(listPage.getTotal());
        return response;
    }

    @PostMapping(value = "addTeamForList")
    public Response add(@RequestBody FkTeamList body) {
        Response<FkTeamList> response = new Response<>();
        Assert.isTrue(this.teamListService.addOne(body) == 1, MSG_ADD_ERROR);
        return response;
    }

    @PostMapping(value = "updateTeamFroList")
    public Response update(@RequestBody FkTeamList body) {
        Response<FkTeamList> response = new Response<>();
        Assert.isTrue(this.teamListService.updateOne(body) == 1, MSG_UPDATE_ERROR);
        return response;
    }

    @PostMapping(value = "remove")
    public Response remove(@RequestBody FkTeamList body) {
        Response<FkTeamList> response = new Response<>();
        Assert.isTrue(this.teamListService.deleteOne(body) == 1, MSG_DELETE_ERROR);
        return response;
    }

}

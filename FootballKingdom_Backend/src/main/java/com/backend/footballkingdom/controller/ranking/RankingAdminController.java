package com.backend.footballkingdom.controller.ranking;

import com.backend.footballkingdom.model.ranking.FkRanking;
import com.backend.footballkingdom.model.ranking.vo.RankingPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.ranking.FkRankingService;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/fk/admin/ranking")
public class RankingAdminController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkRankingService fkRankingService;

    @PostMapping(value = "addRanking")
    public Response addRanking(@RequestBody FkRanking fkRanking){
        Response<FkRanking> rankingResponse = new Response<>();
        Assert.isTrue(fkRankingService.addRanking(fkRanking) == 1, "添加积分记录失败");
        return rankingResponse;
    }

    @PostMapping(value = "updateRanking")
    public Response updateRanking(@RequestBody FkRanking fkRanking){
        Response<FkRanking> rankingResponse = new Response<>();
        Assert.isTrue(fkRankingService.updateRankingByPrimaryKey(fkRanking) == 1, "更新积分记录失败");
        return rankingResponse;
    }

    @PostMapping(value = "deleteRanking")
    public Response deleteRanking(@RequestBody FkRanking fkRanking){
        Response<FkRanking> rankingResponse = new Response<>();
        Assert.isTrue(fkRankingService.deleteRankingByPrimaryKey(fkRanking) == 1, "删除积分记录失败");
        return rankingResponse;
    }

    @PostMapping(value = "getRankingByPageAndExample")
    public Response getRankingByPageAndExample(@RequestBody RankingPageRequest fkRanking){
        Response<Page<FkRanking>> rankingResponse = new Response<>();
        Page<FkRanking> fkRankings=fkRankingService.getRankingByExampleAndPage(fkRanking);
        rankingResponse.setBody(fkRankings);
        rankingResponse.setPageTotalNum(fkRankings.getTotal());
        return rankingResponse;
    }

}

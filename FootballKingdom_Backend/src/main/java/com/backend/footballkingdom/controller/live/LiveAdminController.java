package com.backend.footballkingdom.controller.live;

import com.backend.footballkingdom.model.live.FkLive;
import com.backend.footballkingdom.model.live.vo.LivePageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.live.FkLiveService;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import net.sf.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/fk/admin/live")
public class LiveAdminController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private FkLiveService fkLiveService;

    @PostMapping(value = "addLive")
    public Response addLive(@RequestBody FkLive fkLive) {
        String LiveSource="";
        for (int i = fkLive.getLives().size() - 1; i >= 0; i--) {
            if (StringUtil.isEmpty(fkLive.getLives().get(i).get("liveSource"))){
                fkLive.getLives().remove(fkLive.getLives().get(i));
            }else {
                LiveSource = LiveSource+"  "+fkLive.getLives().get(i).get("liveSource");
            }
        }
        fkLive.setLiveSource(LiveSource);
        fkLive.setLiveUrl(JSONArray.fromObject(fkLive.getLives()).toString());

        Response<FkLive> fkLiveResponse = new Response<>();
        fkLive.setLiveDate(DateUtil.formatDate(DateUtil.getNextDay(DateUtil.parser(fkLive.getLiveDate(),DateUtil.FORMAT_DATE_DEFAULT),1),DateUtil.FORMAT_DATE_DEFAULT));
        fkLive.setImportantLiveFlag(fkLive.getImportantLiveFlag().equals("true")?"1":"0");
        fkLive.setImportantWorldCupFlag(fkLive.getImportantWorldCupFlag().equals("true")?"1":"0");
        fkLive.setRecommendedLiveFlag(fkLive.getRecommendedLiveFlag().equals("true")?"1":"0");
        Assert.isTrue(fkLiveService.addLive(fkLive) == 1, "添加直播失败");
        return fkLiveResponse;
    }

    @PostMapping(value = "updateLive")
    public Response updateLive(@RequestBody FkLive fkLive) {
        String LiveSource="";
        for (int i = fkLive.getLives().size() - 1; i >= 0; i--) {
            if (StringUtil.isEmpty(fkLive.getLives().get(i).get("liveSource"))){
                fkLive.getLives().remove(fkLive.getLives().get(i));
            }else {
                LiveSource = LiveSource+"  "+fkLive.getLives().get(i).get("liveSource");
            }
        }
        fkLive.setLiveSource(LiveSource);
        fkLive.setLiveUrl(JSONArray.fromObject(fkLive.getLives()).toString());

        Response<FkLive> fkLiveResponse = new Response<>();
        fkLive.setLiveDate(DateUtil.formatDate(DateUtil.getNextDay(DateUtil.parser(fkLive.getLiveDate(),DateUtil.FORMAT_DATE_DEFAULT),0),DateUtil.FORMAT_DATE_DEFAULT));
        fkLive.setImportantLiveFlag(fkLive.getImportantLiveFlag().equals("true")?"1":"0");
        fkLive.setImportantWorldCupFlag(fkLive.getImportantWorldCupFlag().equals("true")?"1":"0");
        fkLive.setRecommendedLiveFlag(fkLive.getRecommendedLiveFlag().equals("true")?"1":"0");
        FkLive fkLiveOld=fkLiveService.getOneLivePrimaryKey(fkLive.getId());
        /*if (fkLive.getDelFlag().equals("0")){
            if (fkLive.getImportantWorldCupFlag().equals("1")&&!fkLiveOld.getImportantWorldCupFlag().equals("1")){
                Assert.isTrue(fkLiveService.countImportantWorldCupLive()<3, "世界杯重要直播栏位已满，请重新调配");
            }
            if (fkLive.getImportantLiveFlag().equals("1")&&!fkLiveOld.getImportantLiveFlag().equals("1")){
                Assert.isTrue(fkLiveService.countImportantLive()<3, "轮播直播栏位已满，请重新调配");
            }
        }*/
        Assert.isTrue(fkLiveService.updateLiveByPrimaryKey(fkLive) == 1, "更新直播失败");
        return fkLiveResponse;
    }

    @PostMapping(value = "changeLiveStatus")
    public Response changeLiveStatus(@RequestBody FkLive fkLive) {
        Response<FkLive> fkLiveResponse = new Response<>();
        /*if (fkLive.getDelFlag().equals("0")){
            if (fkLive.getImportantLiveFlag().equals("1")){
                Assert.isTrue(fkLiveService.countImportantLive()<3, "轮播直播栏位已满，请重新调配");
            }
            if (fkLive.getImportantWorldCupFlag().equals("1")){
                Assert.isTrue(fkLiveService.countImportantWorldCupLive()<3, "世界杯重要直播栏位已满，请重新调配");
            }
        }*/
        Assert.isTrue(fkLiveService.updateLiveByPrimaryKey(fkLive) == 1, "改变直播状态失败");
        return fkLiveResponse;
    }

    @PostMapping(value = "deleteLive")
    public Response deleteLive(@RequestBody FkLive fkLive) {
        Response<FkLive> fkLiveResponse = new Response<>();
        Assert.isTrue(fkLiveService.deleteLiveByPrimaryKey(fkLive) == 1, "删除直播失败");
        return fkLiveResponse;
    }

    @PostMapping(value = "getLiveByPageAndExample")
    public Response getLiveByPageAndExample(@RequestBody LivePageRequest livePageRequest) {
        Response<Page<FkLive>> fkLivePageResponse = new Response<>();
        Page<FkLive> livePage = fkLiveService.getLiveByExampleAndPage(livePageRequest);
        fkLivePageResponse.setBody(livePage);
        fkLivePageResponse.setPageTotalNum(livePage.getTotal());
        return fkLivePageResponse;
    }

    @PostMapping(value = "getLiveByExample")
    public Response getLiveByExample(@RequestBody FkLive fkLive) {
        Response<List<FkLive>> fkLiveListResponse = new Response<>();
        List<FkLive> lives = fkLiveService.getLiveByExample(fkLive);
        fkLiveListResponse.setBody(lives);
        return fkLiveListResponse;
    }
}

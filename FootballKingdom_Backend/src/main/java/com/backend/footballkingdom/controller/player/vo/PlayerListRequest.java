package com.backend.footballkingdom.controller.player.vo;

import lombok.Data;

@Data
public class PlayerListRequest {

    private int pageNum;

    private int pageSize;

    private String leagueId;

    private String teamId;

    private String playerName;

    private String playListTime;

}

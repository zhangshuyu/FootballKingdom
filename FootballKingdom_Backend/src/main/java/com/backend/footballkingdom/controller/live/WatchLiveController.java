package com.backend.footballkingdom.controller.live;

import com.backend.footballkingdom.controller.user.vo.MiniUserRequest;
import com.backend.footballkingdom.model.comment.FkComment;
import com.backend.footballkingdom.model.live.FkLive;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.model.team.FkTeam;
import com.backend.footballkingdom.model.user.FkMiniUser;
import com.backend.footballkingdom.service.comment.FkCommentService;
import com.backend.footballkingdom.service.live.FkLiveService;
import com.backend.footballkingdom.service.team.FkTeamService;
import com.backend.footballkingdom.service.user.FkMiniUserService;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.util.StringUtil;
import net.sf.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/fk/watchLive")
public class WatchLiveController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private FkLiveService fkLiveService;

    @Resource
    private FkMiniUserService fkMiniUserService;

    @Resource
    private FkCommentService fkCommentService;

    @Resource
    private FkTeamService fkTeamService;

    @RequestMapping("/watchLiveDetail")
    public String watchLiveDetail(Model model, String  liveId, String openid,String  liveUrl, HttpSession session) {
        Map liveAllInfo=new HashMap<>();
        FkLive live=fkLiveService.getOneLivePrimaryKey(liveId);
        FkTeam mainTeam=fkTeamService.getOneTeamByPrimaryKey(live.getLiveTeamMainId());
        FkTeam visiterTeam=fkTeamService.getOneTeamByPrimaryKey(live.getLiveTeamVisiterId());
        Map<String,Object> x= (Map<String, Object>) JSONArray.fromObject(live.getLiveUrl()).get(Integer.parseInt(liveUrl));
        liveUrl= (String) x.get("liveUrl");

        liveAllInfo.put("live",live);
        liveAllInfo.put("mainTeam",mainTeam);
        liveAllInfo.put("visiterTeam",visiterTeam);
        FkComment fkComment=new FkComment();
        fkComment.setLiveId(liveId);
        model.addAttribute("comments",fkCommentService.getFkCommentByExample(fkComment));
        session.setAttribute("live",liveAllInfo);
        session.setAttribute("liveUrl",liveUrl);
        session.setAttribute("openid",openid);
        return "watchLive";
    }

    @RequestMapping("/addComment")
    @ResponseBody
    public Response addComment(FkComment fkComment){
        Response response = new Response<>();
        fkComment.setId(UUIDUtils.randomUUIDWithoutLine());
        fkComment.setCreateDate(new Date());
        MiniUserRequest request=new MiniUserRequest();
        if (StringUtil.isEmpty(fkComment.getSenderId())){
            response.setStatus(202);
            response.setMessage("对不起，我们无法获取您的信息！请先授权给我们！");
            return response;
        }
        request.setOpenid(fkComment.getSenderId());
        FkMiniUser miniUser=fkMiniUserService.getMiniUserByExample(request).get(0);
        if (miniUser.getRemarks().equals("black")){
            response.setStatus(403);
            response.setMessage("很抱歉！您已经被管理员禁言！");
            return response;
        }
        fkComment.setSenderName(miniUser.getNickName());
        fkComment.setSenderHead(miniUser.getHeadImg());
        if (fkCommentService.addComment(fkComment) != 1){
            response.setStatus(202);
            response.setMessage("提交评论失败，请重试！");
        }
        return response;
    }

    @RequestMapping("/getComments")
    public String getComments(FkComment fkComment,Model model,HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin", "*");
        model.addAttribute("comments",fkCommentService.getFkCommentByExample(fkComment));
        return "watchLive::allComments";
    }

}

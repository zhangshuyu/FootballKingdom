package com.backend.footballkingdom.controller.player;

import com.backend.footballkingdom.controller.player.vo.PlayerListRequest;
import com.backend.footballkingdom.model.playerList.FkPlayerList;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.playerList.FkPlayerListService;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/fk/admin/playerList")
public class PlayerListAdminController {

    @Autowired
    private FkPlayerListService playerListService;

    private static final String MSG_ADD_ERROR = "添加失败";
    private static final String MSG_UPDATE_ERROR = "更新失败";
    private static final String MSG_DELETE_ERROR = "删除失败";

    @PostMapping(value = "getByPage")
    public Response getByPage(@RequestBody PlayerListRequest request) {
        Response<Page<FkPlayerList>> response = new Response<>();
        Page<FkPlayerList> listPage = playerListService.findByPage(request);
        response.setBody(listPage);
        response.setPageTotalNum(listPage.getTotal());
        return response;
    }

    @PostMapping(value = "addPlayerForList")
    public Response add(@RequestBody FkPlayerList body) {
        Response<FkPlayerList> response = new Response<>();
        Assert.isTrue(this.playerListService.addPlayerList(body) == 1, MSG_ADD_ERROR);
        return response;
    }

    @PostMapping(value = "updatePlayerFroList")
    public Response update(@RequestBody FkPlayerList body) {
        Response<FkPlayerList> response = new Response<>();
        Assert.isTrue(this.playerListService.updatePlayerListByPrimaryKey(body) == 1, MSG_UPDATE_ERROR);
        return response;
    }

    @PostMapping(value = "remove")
    public Response remove(@RequestBody FkPlayerList body) {
        Response<FkPlayerList> response = new Response<>();
        Assert.isTrue(this.playerListService.deletePlayerListByPrimaryKey(body) == 1, MSG_DELETE_ERROR);
        return response;
    }

}

package com.backend.footballkingdom.controller.ranking;

import com.backend.footballkingdom.model.ranking.FkRanking;
import com.backend.footballkingdom.model.ranking.vo.RankingPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.ranking.FkRankingService;
import com.backend.footballkingdom.util.DateTime;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/fk/ranking")
public class RankingController {

    @Autowired
    private FkRankingService fkRankingService;

    @Value(value = "${WorldCupLeagueId}")
    private String WorldCupLeagueId;

    @PostMapping(value = "getRankingByPageAndExample")
    public Response getRankingByPageAndExample(@RequestBody RankingPageRequest fkRanking){
        Response<Page<FkRanking>> rankingResponse = new Response<>();
        Page<FkRanking> fkRankings=fkRankingService.getRankingByExampleAndPage(fkRanking);
        rankingResponse.setBody(fkRankings);
        rankingResponse.setPageTotalNum(fkRankings.getPages());
        return rankingResponse;
    }

    @PostMapping(value = "getAllKindsByLeague")
    public List<FkRanking> getAllKindsByLeague(@RequestBody RankingPageRequest fkRanking){
        return fkRankingService.getAllKindsByLeague(fkRanking);
    }

    @GetMapping(value = "getWorldCupRanking")
    @ResponseBody
    public Response getWorldCupRanking(){
        Response<List<FkRanking>> rankingResponse = new Response<>();
        RankingPageRequest rankingPageRequest=new RankingPageRequest();
        rankingPageRequest.setRankingTime("2017/18");
        rankingPageRequest.setLeagueId(WorldCupLeagueId);
        rankingResponse.setBody(fkRankingService.getWorldCupRanking(rankingPageRequest));
        return rankingResponse;
    }



}

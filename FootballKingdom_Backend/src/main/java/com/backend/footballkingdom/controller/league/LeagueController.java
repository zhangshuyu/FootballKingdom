package com.backend.footballkingdom.controller.league;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.league.FkLeagueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/fk/league")
public class LeagueController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkLeagueService leagueService;

    @GetMapping(value = "getAllLeagueNotDelete")
    public Response getAllLeagueNotDelete() {
        Response<List<FkLeague>> fkLeagueListResponse = new Response<>();
        fkLeagueListResponse.setBody(leagueService.getAllLeagueNotDelete());
        return fkLeagueListResponse;
    }
}

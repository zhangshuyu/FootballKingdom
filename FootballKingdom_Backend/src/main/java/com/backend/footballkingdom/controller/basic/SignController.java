package com.backend.footballkingdom.controller.basic;

import com.alibaba.fastjson.JSONObject;
import com.backend.footballkingdom.controller.user.vo.MiniUserRequest;
import com.backend.footballkingdom.enums.WXRequestMethod;
import com.backend.footballkingdom.enums.ZBBRequestMethod;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.model.user.FkAdminUser;
import com.backend.footballkingdom.model.user.FkMiniUser;
import com.backend.footballkingdom.service.user.FkAdminUserService;
import com.backend.footballkingdom.service.user.FkMiniUserService;
import com.backend.footballkingdom.util.DateUtil;
import com.backend.footballkingdom.util.HttpUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Controller
public class SignController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkAdminUserService adminUserService;

    @Autowired
    private FkMiniUserService miniUserService;


    @Value(value = "${MiniAppId}")
    private String MiniAppId;

    @Value(value = "${MiniSecret}")
    private String MiniSecret;


    @RequestMapping(value = "test")
    @ResponseBody
    public Object test(HttpServletResponse response) {
        String GET_DATA_URL = ZBBRequestMethod.GET_DATA_URL.getUrl().replace("LEAGUE", "意甲").replace("TAB", "赛程");
        JSONObject httpResponse = HttpUtils.getHttpResponse(GET_DATA_URL,null);
        logger.info(httpResponse+"");
        return httpResponse;
    }

    @RequestMapping(value = "signOut")
    @ResponseBody
    public Response signOut(HttpServletResponse httpServletResponse) {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        Response response = new Response<>();
        response.setBody("signOut");
        response.setStatus(401);
        return response;
    }

    @RequestMapping(value = "wxGetOpenId")
    @ResponseBody
    public Response wxGetOpenId(@RequestBody wxLoginRequest wxLoginRequest) {
        Response response = new Response<>();
        String MINI_GET_AUTHORIZATION_URL = WXRequestMethod.MINI_GET_AUTHORIZATION_URL.getUrl().replace("APPID", MiniAppId).replace("SECRET", MiniSecret).replace("JSCODE", wxLoginRequest.getCode());
        JSONObject getAuthorizationResponse = HttpUtils.getHttpResponse(MINI_GET_AUTHORIZATION_URL, null);
        Assert.isTrue(!getAuthorizationResponse.containsKey("errcode"), "获取用户信息失败：" + getAuthorizationResponse);
        MiniUserRequest miniUser = new MiniUserRequest();
        String openid = (String) getAuthorizationResponse.get("openid");
        miniUser.setOpenid(openid);
        List<FkMiniUser> miniUsersByOpenid = miniUserService.getMiniUserByExample(miniUser);
        response.setBody(openid);
        if (miniUsersByOpenid.size() > 0) {
            logger.info("已存在该用户");
            response.setStatus(203);
        }
        return response;
    }

    @RequestMapping(value = "wxGetUserInfo")
    @ResponseBody
    public Response wxGetUserInfo(@RequestBody FkMiniUser miniUser) {
        Response response = new Response<>();
        if (StringUtil.isNotEmpty(miniUser.getNickName())){
            miniUser.setCreateDate(DateUtil.formatDate(new Date(),DateUtil.FORMAT_DATE_DEFAULT));
            miniUserService.addMiniUser(miniUser);
        }else {
            response.setStatus(403);
        }
        return response;
    }

    @PostMapping(value = "adminLogin")
    @ResponseBody
    public Response<FkAdminUser> adminUserLogin(@RequestBody FkAdminUser adminUser) {

        Response<FkAdminUser> fkAdminUserResponse = new Response<FkAdminUser>();

        UsernamePasswordToken token = new UsernamePasswordToken(adminUser.getEmail(), adminUser.getPassword());
        //获取用户（不一定是登录的用户）
        Subject currentUser = SecurityUtils.getSubject();
        try {
            //校验当前用户是否登录成功，如果登录不成功该方法会抛出一些指定的异常
            //这里会调用AuthorizingRealm类当中的doGetAuthenticationInfo(AuthenticationToken token)方法
            currentUser.login(token);
            currentUser.getSession().setTimeout(3600L * 1000 * 24);
            fkAdminUserResponse.setBody(adminUser);
            adminUser.setUserRole(currentUser.hasRole("admin") ? "0" : "1");
        } catch (AuthenticationException e) {
            logger.error("用户登录不成功", e);
            fkAdminUserResponse.setMessage("用户" + adminUser.getEmail() + "登陆不成功，原因：" + e.getMessage());
            fkAdminUserResponse.setStatus(401);
        }
        return fkAdminUserResponse;
    }
}

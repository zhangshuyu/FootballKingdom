package com.backend.footballkingdom.controller.games;

import com.backend.footballkingdom.model.games.FkGames;
import com.backend.footballkingdom.model.games.GamesPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.games.FkGamesService;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/fk/admin/games")
public class GamesAdminController {
    @Resource
    private FkGamesService fkGamesService;

    @PostMapping(value = "/getAllGames")
    public Response getAllGames(@RequestBody GamesPageRequest gamesPageRequest) {
        Response<List<FkGames>> response = new Response<>();
        List<FkGames> list = fkGamesService.findGamesWithLeagueAndTeam(gamesPageRequest);
        response.setPageTotalNum(((Page)list).getTotal());
        response.setBody(list);
        return response;
    }

    @PostMapping(value = "/addGame")
    public Response addGame(@RequestBody FkGames fkGames) {
        Response response = new Response();
        String datetimeStr = fkGames.getGameDate() +" "+ fkGames.getGameTime();
        Date dateTime = DateUtil.parser(datetimeStr,DateUtil.FORMAT_DATETIME_YYYY_MM_DD_HH_MM);
        fkGames.setGameDatetime(dateTime);
        Assert.isTrue(fkGamesService.addGames(fkGames) == 1, "添加赛程失败");
        return response;
    }

    @PostMapping(value = "/deleteGame")
    public Response deleteGameById(@RequestBody FkGames fkGames) {
        Response response = new Response();
        Assert.isTrue(fkGamesService.deleteGamesByPrimaryKey(fkGames)==1,"删除赛程失败");
        return response;
    }

    @PostMapping(value = "/updateGame")
    public Response updateGame(@RequestBody FkGames fkGames) {
        String datetimeStr = fkGames.getGameDate() +" "+ fkGames.getGameTime();
        Date dateTime = DateUtil.parser(datetimeStr,DateUtil.FORMAT_DATETIME_YYYY_MM_DD_HH_MM);
        fkGames.setGameDatetime(dateTime);
        Response response = new Response();
        Assert.isTrue(fkGamesService.updateGamesByPrimaryKey(fkGames)==1,"更新失败");
        return response;
    }

}

package com.backend.footballkingdom.controller.news;

import com.backend.footballkingdom.model.news.FkNews;
import com.backend.footballkingdom.model.news.vo.NewsPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.news.FkNewsService;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @author zh.d
 */
@RestController
@RequestMapping(value = "/fk/admin/newsManager")
public class NewsAdminController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String REMARK = "true";
    private static final String ONE = "1";
    private static final String ZERO = "0";
    private static final String MSG_ONE = "世界杯重要新闻栏位已满，请重新配置";
    private static final String MSG_TWO = "添加新闻失败";
    private static final String MSG_THREE = "更新新闻失败";
    private static final String MSG_FOUR = "删除新闻失败";

    @Autowired
    private FkNewsService newsService;

    @PostMapping(value = "addNews")
    public Response addNews(@RequestBody FkNews news) {
        news.setCreateDate(DateUtil.formatDate(new Date(), DateUtil.FORMAT_DATETIME_YYYY_MM_DD_HH_MM));
        Response<FkNews> response = new Response<>();
        if(ONE.equals(news.getImportantWorldCupFlag())) {
            Assert.isTrue(newsService.countImportantWorldCupNews() < 3, MSG_ONE);
        }
        Assert.isTrue(newsService.addNews(news) == 1, MSG_TWO);
        return response;
    }

    @PostMapping(value = "updateNews")
    public Response updateNews(@RequestBody FkNews news) {
        //news.setImportantNewsFlag(REMARK.equals(news.getImportantNewsFlag())?ONE:ZERO);
        //news.setImportantWorldCupFlag(REMARK.equals(news.getImportantWorldCupFlag())?ONE:ZERO);
        //news.setRecommendedNewsFlag(REMARK.equals(news.getRecommendedNewsFlag())?ONE:ZERO);
        Response<FkNews> response = new Response<>();
        if(ONE.equals(news.getImportantWorldCupFlag())) {
            Assert.isTrue(newsService.countImportantWorldCupNews() < 3, MSG_ONE);
        }
        Assert.isTrue(newsService.updateNews(news) == 1, MSG_THREE);
        return response;
    }

    @PostMapping(value = "getNewsByPage")
    public Response getNewsByPage(@RequestBody NewsPageRequest page) {
        Response<Page<FkNews>> response = new Response<>();
        Page<FkNews> news = newsService.getNewsByPage(page);
        response.setBody(news);
        response.setPageTotalNum(news.getTotal());
        return response;
    }

    @PostMapping(value = "remove")
    public Response remove(@RequestBody FkNews fkNews) {
        Response<FkNews> response = new Response<>();
        Assert.isTrue(newsService.removeWithDelFag(fkNews) == 1, MSG_FOUR);
        return response;
    }

    @PostMapping(value = "getNewsWithWorldCup")
    public Response getNewsWithWorldCup() {
        Response<List<FkNews>> response = new Response<>();
        List<FkNews> news = newsService.findWorldCupNews();
        response.setBody(news);
        return response;
    }

    @PostMapping(value = "getNewById")
    public Response getNewById(@RequestBody FkNews fkNews) {
        Response<FkNews> response = new Response<>();
        FkNews news = newsService.findById(fkNews.getId());
        response.setBody(news);
        return response;
    }

}
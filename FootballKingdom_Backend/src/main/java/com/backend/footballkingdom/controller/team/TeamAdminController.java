package com.backend.footballkingdom.controller.team;

import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.model.team.FkTeam;
import com.backend.footballkingdom.service.team.FkTeamService;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/fk/admin/team")
public class TeamAdminController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkTeamService teamService;

    @PostMapping(value = "getAllTeam")
    private Response getAllTeam(){
        Response<List<FkTeam>>  fkTeamListResponse=new Response<>();
        fkTeamListResponse.setBody(teamService.getAllTeamNotDelete());
        return fkTeamListResponse;
    }



    @PostMapping(value = "getAllTeamByPage")
    private Response getAllTeamByPage(@RequestBody Map<String, Integer> page){
        Response<Page<FkTeam>>  fkTeamPageResponse=new Response<>();
        Page<FkTeam> teamPage=teamService.getAllTeamByPage(page.get("teamPageNum"), page.get("teamPageSize"));
        fkTeamPageResponse.setBody(teamPage);
        fkTeamPageResponse.setPageTotalNum(teamPage.getTotal());
        return fkTeamPageResponse;
    }

    @PostMapping(value = "addTeam")
    public Response addTeam(@RequestBody FkTeam fkTeam){
        Response<FkTeam>  fkTeamResponse=new Response<>();
        Assert.isTrue(teamService.addTeam(fkTeam) == 1, "添加球队失败");
        return fkTeamResponse;
    }

    @PostMapping(value = "updateTeam")
    public Response updateTeam(@RequestBody FkTeam fkTeam){
        Response<FkTeam>  fkTeamResponse=new Response<>();
        Assert.isTrue(teamService.updateTeamByPrimaryKey(fkTeam) == 1, "更新球队失败");
        return fkTeamResponse;
    }

    @PostMapping(value = "deleteTeam")
    public Response deleteTeam(@RequestBody FkTeam fkTeam){
        Response<FkTeam>  fkTeamResponse=new Response<>();
        Assert.isTrue(teamService.deleteTeamByPrimaryKey(fkTeam) == 1, "删除球队失败");
        return fkTeamResponse;
    }


}

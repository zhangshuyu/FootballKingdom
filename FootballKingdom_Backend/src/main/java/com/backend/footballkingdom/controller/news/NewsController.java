package com.backend.footballkingdom.controller.news;

import com.backend.footballkingdom.model.news.FkNews;
import com.backend.footballkingdom.model.news.vo.NewsPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.news.FkNewsService;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/fk/newsManager")
public class NewsController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkNewsService newsService;

    @PostMapping(value = "getNewsByExamle")
    @ResponseBody
    public Response getNewsByExamle(@RequestBody NewsPageRequest news) {
        Response<Page<FkNews>> response = new Response<>();
        Page<FkNews> page=newsService.getNewsByExamle(news);
        response.setBody(page);
        response.setPageTotalNum(page.getPages());
        return response;
    }

    @GetMapping(value = "getWorldCupNews")
    @ResponseBody
    public Response getNewsByWorld() {
        Response<List<FkNews>> response = new Response<>();
        response.setBody(newsService.findWorldCupNews());
        return response;
    }

    @PostMapping(value = "getWorldById")
    @ResponseBody
    public Response getNewsById(@RequestBody NewsPageRequest news){
        Response<FkNews> response = new Response<>();
        response.setBody(newsService.findById(news.getId()));
        return response;
    }

    /*@RequestMapping(value = "view")
    public String viewNews(Model model) {
        //if(news.getId() != null) {
        //FkNews getNews = this.newsService.findById(news.getId());
        FkNews getNews = this.newsService.findById("4ae1c7bfa3194a14819ee8d8563c602b");
        model.addAttribute("news", getNews);
        //}
        return "viewNews";
    }*/

}

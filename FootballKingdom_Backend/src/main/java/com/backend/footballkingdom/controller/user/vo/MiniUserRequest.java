package com.backend.footballkingdom.controller.user.vo;

import lombok.Data;

@Data
public class MiniUserRequest {
    private int pageNum;
    private int pageSize;
    private String createDate;
    private String openid;
    private String nickName;
}

package com.backend.footballkingdom.controller.live;

import com.backend.footballkingdom.model.live.FkLive;
import com.backend.footballkingdom.model.live.vo.LivePageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.live.FkLiveService;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/fk/live")
public class LiveController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private FkLiveService fkLiveService;

    @Value(value = "${WorldCupLeagueId}")
    private String WorldCupLeagueId;

    @PostMapping(value = "getLiveByExampleAllInfo")
    public Response getLiveByExample(@RequestBody LivePageRequest livePageRequest) {
        Response<List<FkLive>> fkLiveResponse = new Response<>();
        List<FkLive> lives = fkLiveService.getLiveByExampleAndPage(livePageRequest);
        fkLiveResponse.setBody(lives);
        return fkLiveResponse;
    }

    @PostMapping(value = "getLiveById")
    public Response getLiveById(@RequestBody LivePageRequest livePageRequest) {
        Response fkLiveResponse = new Response<>();
        FkLive live = fkLiveService.getOneLivePrimaryKey(livePageRequest.getId());
        fkLiveResponse.setBody(live);
        return fkLiveResponse;
    }

    @PostMapping(value = "getLiveByPageAndExampleAllInfo")
    public Response getLiveByPageAndExampleAllInfo(@RequestBody LivePageRequest livePageRequest) {
        livePageRequest.getLiveDate();
        Response<Page<FkLive>> fkLiveResponse = new Response<>();
        Page<FkLive> lives = fkLiveService.getLiveByExampleAndPage(livePageRequest);
        fkLiveResponse.setBody(lives);
        fkLiveResponse.setPageTotalNum(lives.getPages());
        return fkLiveResponse;
    }

    @GetMapping(value = "getWorldCupLive")
    public Response getWorldCupLive() {
        FkLive fkLive=new FkLive();
        fkLive.setDelFlag("0");
        fkLive.setLeagueId(WorldCupLeagueId);
        fkLive.setImportantWorldCupFlag("1");
        Response<List<FkLive>> fkLiveListResponse = new Response<>();
        List<FkLive> lives = fkLiveService.getLiveByExample(fkLive);
        fkLiveListResponse.setBody(lives);
        return fkLiveListResponse;
    }
}

package com.backend.footballkingdom.controller.games;


import com.backend.footballkingdom.model.games.FkGames;
import com.backend.footballkingdom.model.games.GamesPageRequest;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.games.FkGamesService;
import com.backend.footballkingdom.util.DateUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/fk/games")
public class GamesController {
    @Resource
    private FkGamesService fkGamesService;

    @Value(value = "${WorldCupLeagueId}")
    private String WorldCupLeagueId;

    @PostMapping(value = "/getWorldCupGames")
    public Response getWorldCupGames(@RequestBody GamesPageRequest gamesPageRequest){
        Response<List<FkGames>> response = new Response<>();
        gamesPageRequest.setGameStartTime(DateUtil.formatDate(new Date(),DateUtil.FORMAT_DATE_DEFAULT));
        gamesPageRequest.setSleagueId(WorldCupLeagueId);
        List<FkGames> fkGames=fkGamesService.findGamesByStartDayWithLeagueAndTeam(gamesPageRequest);
        response.setBody(fkGames);
        response.setPageTotalNum(((Page)fkGames).getPages());
        return response;
    }

    @PostMapping(value = "/getGameById")
    public Response getGameById(@RequestBody GamesPageRequest gamesPageRequest){
        Response<List<FkGames>> response = new Response<>();
        List<FkGames> fkGames=fkGamesService.findGameByIdWithLeagueAndTeam(gamesPageRequest);
        response.setBody(fkGames);
        return response;
    }
}

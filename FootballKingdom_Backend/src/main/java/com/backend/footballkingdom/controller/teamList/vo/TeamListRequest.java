package com.backend.footballkingdom.controller.teamList.vo;

import lombok.Data;

/**
 * @author: zh.d
 * @description:
 * @Date: Create at 17:42 2018/6/7
 */
@Data
public class TeamListRequest {

    private int pageNum;

    private int pageSize;

    private String leagueId;

    private String teamId;

    private String teamListTime;

}

package com.backend.footballkingdom.controller.league;

import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.model.response.Response;
import com.backend.footballkingdom.service.league.FkLeagueService;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/fk/admin/league")
public class LeagueAdminController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkLeagueService leagueService;

    @PostMapping(value = "addLeague")
    public Response addLeague(@RequestBody FkLeague fkLeague) {
        Response<FkLeague> fkLeagueResponse = new Response<>();
        Assert.isTrue(leagueService.addLeague(fkLeague) == 1, "添加联赛失败");
        return fkLeagueResponse;
    }

    @PostMapping(value = "getAllLeagueNotDelete")
    public Response getAllLeagueNotDelete() {
        Response<List<FkLeague>> fkLeagueListResponse = new Response<>();
        fkLeagueListResponse.setBody(leagueService.getAllLeagueNotDelete());
        return fkLeagueListResponse;
    }

    @PostMapping(value = "getAllLeagueByPage")
    public Response getAllLeagueByPage(@RequestBody Map<String, Integer> page) {
        Response<Page<FkLeague>> fkLeaguePageResponse = new Response<>();
        Page<FkLeague> leaguePage=leagueService.getAllLeagueByPage(page.get("leaguePageNum"), page.get("leaguePageSize"));
        fkLeaguePageResponse.setBody(leaguePage);
        fkLeaguePageResponse.setPageTotalNum(leaguePage.getTotal());
        return fkLeaguePageResponse;
    }

    @PostMapping(value = "updateLeague")
    public Response updateLeague(@RequestBody FkLeague fkLeague) {
        Response<FkLeague> fkLeagueResponse = new Response<>();
        Assert.isTrue(leagueService.updateLeagueByPrimaryKey(fkLeague) == 1, "更新联赛失败");
        return fkLeagueResponse;
    }

    @PostMapping(value = "deleteLeague")
    public Response deleteLeague(@RequestBody FkLeague fkLeague) {
        Response<FkLeague> fkLeagueResponse = new Response<>();
        Assert.isTrue(leagueService.deleteLeagueByPrimaryKey(fkLeague) == 1, "删除联赛失败");
        return fkLeagueResponse;
    }
}

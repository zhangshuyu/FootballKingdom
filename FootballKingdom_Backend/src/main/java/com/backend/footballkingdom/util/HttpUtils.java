package com.backend.footballkingdom.util;

import com.alibaba.fastjson.JSONObject;
import com.backend.footballkingdom.enums.WXRequestMethod;
import com.backend.footballkingdom.model.accessToken.AccessToken;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Assert;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Slf4j
public class HttpUtils {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    protected static RestTemplate getRestTemplate(){
        RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        template.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return false;
            }
            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
            }
        });
        return  template;
    }

    public static JSONObject getHttpResponse(String url,Object req) {
        RestTemplate restTemplate = getRestTemplate();
        HttpEntity<String> response=restTemplate.postForEntity(url,req,String.class);
        return JSONObject.parseObject(response.getBody());
    }

    public static AccessToken getAccessToken(String APPID, String APPSECRET) {
        AccessToken accessTokenResp = new AccessToken();
        String ACCESS_TOKEN_URL = WXRequestMethod.ACCESS_TOKEN_URL.getUrl().replace("APPID", APPID).replace("APPSECRET", APPSECRET);
        JSONObject httpResponse = getHttpResponse(ACCESS_TOKEN_URL,null);

        Assert.isTrue(!httpResponse.containsKey("errcode"),"获取AccessToken失败:"+httpResponse);

        accessTokenResp.setToken(httpResponse.getString("access_token"));
        accessTokenResp.setExpiresIn(httpResponse.getInteger("expires_in"));

        return accessTokenResp;
    }

}

package com.backend.footballkingdom.util;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class DateUtil {

    protected static Map<String, DateFormat> dateFormatMap = new HashMap();
    public static final String FORMAT_DATE_DEFAULT = "yyyy-MM-dd";
    public static final String FORMAT_DATE_YYYYMMDD = "yyyyMMdd";
    public static final String FORMAT_DATE_YYYYMMDDHH = "yyyyMMddHH";
    public static final String FORMAT_DATE_YYYYMM = "yyyyMM";
    public static final String FORMAT_DATE_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMAT_DATE_SLASH_YYYY_MM_DD = "yyyy/MM/dd";
    public static final String FORMAT_DATE_SLASH_YYYY_M_DD = "yyyy/M/dd";
    public static final String FORMAT_DATETIME_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FORMAT_DATETIME_YYYY_MM_DD_HHMM = "yyyy-MM-dd HHmm";
    public static final String FORMAT_DATETIME_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_DATETIME_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String FORMAT_DATETIME_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
    public static final String FORMAT_TIME_DEFAULT = "HH:mm:ss";
    public static final String FORMAT_TIME_HH_MM = "HH:mm";
    public static final String FORMAT_TIME_HHMM = "HHmm";
    public static final String FORMAT_TIME_HH_MM_SS = "HH:mm:ss";
    public static final String FORMAT_TIME_HHMMSS = "HHmmss";
    public static final String FORMAT_TIME_HHMMSS_SSS = "HHmmssSSS";

    protected static DateFormat getCachedDateFormat(String formatPattern) {
        DateFormat dateFormat = dateFormatMap.get(formatPattern);
        if (dateFormat == null) {
            dateFormat = getDateFormat(formatPattern);
            dateFormatMap.put(formatPattern, getDateFormat(formatPattern));
        }

        return dateFormat;
    }

    public static String formatDate(Date date, String formatPattern) {
        return date == null ? null : getCachedDateFormat(formatPattern).format(date);
    }

    public static String formatDefaultDate(Date date) {
        return date == null ? null : getCachedDateFormat("yyyy-MM-dd").format(date);
    }

    public static DateFormat getDateFormat(String formatPattern) {
        return new SimpleDateFormat(formatPattern);
    }

    public static Date parser(String dateStr, String formatPattern) {
        try {
            return getDateFormat(formatPattern).parse(dateStr);
        } catch (ParseException var3) {
            return null;
        }
    }

    public static Date getNextYear(Date date, int yearNum) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(1, calendar.get(1) + yearNum);
        calendar.set(6, calendar.get(6));
        return calendar.getTime();
    }

    public static Date getNextDateByMonth(Date date, int monthNum) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(2, calendar.get(2) + monthNum);
        calendar.set(6, calendar.get(6));
        return calendar.getTime();
    }

    public static Date getNextDay(Date date, int dayNum) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(6, calendar.get(6) + dayNum);
        return calendar.getTime();
    }

    public static long getBetweenDay(Date startDate, Date endDate) {
        Calendar d1 = Calendar.getInstance();
        d1.setTime(startDate);
        Calendar d2 = Calendar.getInstance();
        d2.setTime(endDate);
        long days = (d2.getTime().getTime() - d1.getTime().getTime()) / 86400000L;
        return days;
    }

    private long differenceOfMonths;//月份差值
    private long differenceOfDays;//天数差值

    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    public static DateUtil calculate(String startdate, String endDate) {
        try {
            return calculate(dateFormat.parse(startdate), dateFormat.parse(endDate));
        } catch (ParseException e) {
            log.error("ParseException:{}", e);
        }
        return null;
    }

    private long getDifferenceOfMonths() {
        return differenceOfMonths;
    }

    private void setDifferenceOfMonths(long differenceOfmonths) {
        this.differenceOfMonths = differenceOfmonths;
    }

    public long getDifferenceOfDays() {
        return differenceOfDays;
    }

    private void setDifferenceOfDays(long differenceOfDays) {
        this.differenceOfDays = differenceOfDays;
    }

    /**
     * 计算差值,注意 endDate > startDate
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static DateUtil calculate(Date startDate, Date endDate) {
        if (startDate.after(endDate)) {
            return null;
        }
        log.info("开始日：" + dateFormat.format(startDate) + ", 结束日: " + dateFormat.format(endDate));
        DateUtil dataCalculate = new DateUtil();

        Calendar firstDay = Calendar.getInstance();
        Calendar lastDay = Calendar.getInstance();
        firstDay.setTime(startDate);
        lastDay.setTime(endDate);

        //算出天数总差值
        long allDays = ((lastDay.getTimeInMillis()) - (firstDay.getTimeInMillis())) / (1000 * 24 * 60 * 60);

        Calendar loopEndDay = calculateLoopEndOfDate(firstDay, lastDay);
        log.info("循环终止日期 : " + dateFormat.format(loopEndDay.getTime()));

        dataCalculate.setDifferenceOfDays(0);
        dataCalculate.setDifferenceOfMonths(0);

        int month = firstDay.get(Calendar.MONTH);
        while (!firstDay.equals(loopEndDay)) {
            firstDay.add(Calendar.DAY_OF_MONTH, 1);
            allDays--;
            if (month != firstDay.get(Calendar.MONTH)) {
                month = firstDay.get(Calendar.MONTH);
                dataCalculate.setDifferenceOfMonths(dataCalculate.getDifferenceOfMonths() + 1);
            }
        }
        dataCalculate.setDifferenceOfDays(allDays);
        return dataCalculate;

    }

    /**
     * 计算循环终止日期
     * 例如:开始日：2011/03/17    结束日 2012/02/13 ,循环终止日期 2012/01/17;
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static Calendar calculateLoopEndOfDate(Calendar startDate, Calendar endDate) {
        int year = endDate.get(Calendar.YEAR);
        int month = endDate.get(Calendar.MONTH);
        int day = startDate.get(Calendar.DAY_OF_MONTH);
        int maxDaysInMonth = getMaxDaysOfMonth(new GregorianCalendar(year, month, 1));

        if (year > startDate.get(Calendar.YEAR)) {
            if (month == Calendar.JANUARY) {
                year -= 1;
                month = Calendar.DECEMBER;
            } else {
                if (day > maxDaysInMonth) {
                    month -= 1;
                    endDate.set(year, month, 1);
                    day = getMaxDaysOfMonth(new GregorianCalendar(year, month, 1));
                } else {
                    if (day > endDate.get(Calendar.DAY_OF_MONTH)) {
                        month -= 1;
                        endDate.set(year, month, 1);
                        maxDaysInMonth = getMaxDaysOfMonth(new GregorianCalendar(year, month, 1));
                        ;
                        if (day > maxDaysInMonth) {
                            day = maxDaysInMonth;
                        }
                    }
                }
            }
        } else {
            if (day > maxDaysInMonth) {
                month -= 1;
                endDate.set(year, month, 1);
                day = getMaxDaysOfMonth(new GregorianCalendar(year, month, 1));
            } else {
                if (day > endDate.get(Calendar.DAY_OF_MONTH)) {
                    month -= 1;
                    endDate.set(year, month, 1);
                    maxDaysInMonth = getMaxDaysOfMonth(new GregorianCalendar(year, month, 1));
                    if (day > maxDaysInMonth) {
                        day = maxDaysInMonth;
                    }
                }
            }
        }

        return new GregorianCalendar(year, month, day);
    }

    /**
     * 获取一月最大天数,考虑年份是否为润年
     * (对API中的 getMaximum(int field)不了解, date.getMaximum(Calendar.DAY_OF_MONTH)却不是月份的最大天数)
     *
     * @param date
     * @return
     */
    private static int getMaxDaysOfMonth(GregorianCalendar date) {
        int month = date.get(Calendar.MONTH);
        int maxDays = 0;
        switch (month) {
            case Calendar.JANUARY:
            case Calendar.MARCH:
            case Calendar.MAY:
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.OCTOBER:
            case Calendar.DECEMBER:
                maxDays = 31;
                break;
            case Calendar.APRIL:
            case Calendar.JUNE:
            case Calendar.SEPTEMBER:
            case Calendar.NOVEMBER:
                maxDays = 30;
                break;
            case Calendar.FEBRUARY:
                if (date.isLeapYear(date.get(Calendar.YEAR))) {
                    maxDays = 29;
                } else {
                    maxDays = 28;
                }
                break;
            default:
                break;
        }
        return maxDays;
    }

    public static long getBetweenMonths(Date startDate, Date endDate) {
        DateUtil dateCalculate = calculate(startDate, endDate);
        return dateCalculate.differenceOfMonths + (dateCalculate.differenceOfDays > 0 ? 1L : 0L);
    }

    public static long getBetweenMonths(String startDate, String endDate) {
        DateUtil dateCalculate = calculate(startDate, endDate);
        return dateCalculate.differenceOfMonths + (dateCalculate.differenceOfDays > 0 ? 1L : 0L);
    }


    /**
     * 格式化带时区的字符串日期,并同时添加8小时
     *
     * @param dateStr 2016-08-29T07:34:31Z
     * @return 2016-08-29 15:34:31
     */
    public static String formatTimeZoneDate(String dateStr) {
        dateStr = dateStr.replaceAll("T", " ");
        dateStr = dateStr.replaceAll("Z", "");

        Date date = DateUtil.parser(dateStr, DateUtil.FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS);
        date = changeTimeZone(date, TimeZone.getTimeZone("GMT"), TimeZone.getTimeZone("Asia/Shanghai"));
        return formatDate(date, DateUtil.FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS);
    }

    /**
     * 获取更改时区后的日期
     *
     * @param date    日期
     * @param oldZone 旧时区对象
     * @param newZone 新时区对象
     * @return 日期
     */
    public static Date changeTimeZone(Date date, TimeZone oldZone, TimeZone newZone) {
        Date dateTmp = null;
        if (date != null) {
            int timeOffset = oldZone.getRawOffset() - newZone.getRawOffset();
            dateTmp = new Date(date.getTime() - timeOffset);
        }
        return dateTmp;
    }

    /**
     * 获取两个时间的秒数差异
     *
     * @param startDate 开始时间
     * @param endDate   终止时间
     * @return 秒数差，取绝对值
     */
    public static long getBetweenSeconds(Date startDate, Date endDate) {
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        long millis = Math.abs(end.getTimeInMillis() - start.getTimeInMillis());
        return millis / 1000;
    }
}

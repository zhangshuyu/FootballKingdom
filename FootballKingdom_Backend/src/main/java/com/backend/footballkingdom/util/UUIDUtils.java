package com.backend.footballkingdom.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.UUID;

/**
 * UUID工具类，可产生随机字符串
 *
 */
@Slf4j
public class UUIDUtils {

	/**
	 * 生成UUID
	 *
	 * @return
	 */
	public static String randomUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 生成UUID并删除中横线
	 *
	 * @return
	 */
	public static String randomUUIDWithoutLine() {
		return randomUUID().replaceAll("-", "");
	}

	/**
	 * 通过UUID生成随机字符串，建议超过32位以保证唯一性
	 *
	 * @param length
	 * @return
	 */
	public static String randomText(final Integer length) {
		Assert.notNull(length, "length is not specified");
		Assert.isTrue(length > 0, "length must be greater than 0");
		StringBuilder text = new StringBuilder();
		while (text.length() < length) {
			text.append(randomUUIDWithoutLine());
		}
		return StringUtils.substring(text.toString(), 0, length);
	}

	public static void main(String[] args) {
//		log.info(randomText(64));
		System.out.println(randomUUID());
	}

}

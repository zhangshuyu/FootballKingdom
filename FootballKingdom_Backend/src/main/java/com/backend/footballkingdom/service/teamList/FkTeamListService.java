package com.backend.footballkingdom.service.teamList;

import com.backend.footballkingdom.controller.teamList.vo.TeamListRequest;
import com.backend.footballkingdom.model.teamList.FkTeamList;
import com.github.pagehelper.Page;

/**
 * @author: zh.d
 * @description:
 * @Date: Create at 17:44 2018/6/7
 */
public interface FkTeamListService {

    Page<FkTeamList> findByPage(TeamListRequest request);

    int addOne(FkTeamList teamList);

    int updateOne(FkTeamList teamList);

    int deleteOne(FkTeamList teamList);

}

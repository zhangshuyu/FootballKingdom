package com.backend.footballkingdom.service.news.impl;

import com.backend.footballkingdom.mapper.FkNewsMapper;
import com.backend.footballkingdom.model.news.FkNews;
import com.backend.footballkingdom.model.news.vo.NewsPageRequest;
import com.backend.footballkingdom.model.team.FkTeam;
import com.backend.footballkingdom.service.news.FkNewsService;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FkNewsServiceImpl implements FkNewsService {

    @Resource
    private FkNewsMapper fkNewsMapper;

    private static final String ONE = "1";
    private static final String ZERO = "0";

    @Override
    @Transactional
    public int addNews(FkNews news) {
        news.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkNewsMapper.insertSelective(news);
    }

    @Override
    @Transactional
    public int removeWithDelFag(FkNews fkNews) {
        int i = fkNewsMapper.deleteByPrimaryKey(fkNews);
        return i;
    }

    @Override
    @Transactional
    public int updateNews(FkNews fkNews) {
        int i = fkNewsMapper.updateByPrimaryKeySelective(fkNews);
        return i;
    }

    @Override
    public int countImportantWorldCupNews() {
        Example example = new Example(FkNews.class);
        example.createCriteria().andEqualTo("importantWorldCupFlag", ONE).andEqualTo("delFlag", ZERO);
        return fkNewsMapper.selectCountByExample(example);
    }

    @Override
    public Page<FkNews> getNewsByPage(NewsPageRequest page) {
        PageHelper.startPage(page.getNewsPageNum(), page.getNewsPageSize());
        List<FkNews> list = new ArrayList<>();
        list = fkNewsMapper.getAllNewsWithLeague(page);
        Page<FkNews> news = (Page<FkNews>) list;
        return news;
    }

    @Override
    public Page<FkNews> getNewsByExamle(NewsPageRequest fkNews) {
        PageHelper.startPage(fkNews.getNewsPageNum(), fkNews.getNewsPageSize());
        Example example = new Example(FkNews.class);
        example.selectProperties("id", "newsTitle",
                "newsTitle2", "newsBigPic", "recommendedNewsFlag",
                "newsUrl", "importantNewsFlag", "importantWorldCupFlag", "newsPic1",
                "newsPic2", "newsPic3", "newsPic4", "newsPic5", "newsDescription"
                , "createBy", "createDate", "updateBy", "updateDate", "remarks", "delFlag");
        example.orderBy("createDate").desc();
        Example.Criteria criteria = example.createCriteria();
        if (StringUtil.isNotEmpty(fkNews.getDelFlag())) {
            criteria.andEqualTo("delFlag", fkNews.getDelFlag());
        }
        if (StringUtil.isNotEmpty(fkNews.getLeagueId())) {
            criteria.andEqualTo("leagueId", fkNews.getLeagueId());
        }
        if (StringUtil.isNotEmpty(fkNews.getImportantNewsFlag())) {
            criteria.andEqualTo("importantNewsFlag", fkNews.getImportantNewsFlag());
        }
        if (StringUtil.isNotEmpty(fkNews.getRecommendedNewsFlag())) {
            criteria.andEqualTo("recommendedNewsFlag", fkNews.getRecommendedNewsFlag());
        }
        Page<FkNews> news = (Page<FkNews>) fkNewsMapper.selectByExample(example);
        return news;
    }

    @Override
    public List<FkNews> findWorldCupNews() {
        List<FkNews> list = new ArrayList<>();
        Example example = new Example(FkNews.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("delFlag", ZERO);
        criteria.andEqualTo("importantWorldCupFlag", ONE);
        list = this.fkNewsMapper.selectByExample(example);
        return list;
    }

    @Override
    public FkNews findById(String id) {
        FkNews news = new FkNews();
        NewsPageRequest request = new NewsPageRequest();
        request.setId(id);
        news = this.fkNewsMapper.selectById(request);
        return news;
    }
}

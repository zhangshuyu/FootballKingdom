package com.backend.footballkingdom.service.comment;

import com.backend.footballkingdom.mapper.FkCommentMapper;
import com.backend.footballkingdom.mapper.FkMiniUserMapper;
import com.backend.footballkingdom.model.comment.FkComment;
import com.backend.footballkingdom.model.user.FkMiniUser;
import com.backend.footballkingdom.util.UUIDUtils;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import com.google.common.collect.Lists;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;

@Service
public class FkCommentService {

    @Resource
    private FkCommentMapper commentMapper;

    @Resource
    private FkMiniUserMapper miniUserMapper;

    @Transactional
    public int addComment(FkComment fkComment) {
        fkComment.setId(UUIDUtils.randomUUIDWithoutLine());
        return commentMapper.insertSelective(fkComment);
    }

    @Transactional
    public int deleteCommentByPrimaryKey(FkComment fkComment) {
        return commentMapper.deleteByPrimaryKey(fkComment);
    }

    @Transactional
    public int updateComment(FkComment fkComment) {
        return commentMapper.updateByPrimaryKeySelective(fkComment);
    }

    public List<FkComment> getFkCommentByExample(FkComment fkComment) {
        Example example = new Example(FkComment.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtil.isNotEmpty(fkComment.getLiveId())) {
            criteria.andEqualTo("liveId", fkComment.getLiveId());
        }
        if (StringUtil.isNotEmpty(fkComment.getRemarks()) && "newest".equals(fkComment.getRemarks())) {
            example.orderBy("createDate").desc();
        } else {
            example.orderBy("createDate").asc();
        }
        List<FkComment> comments = commentMapper.selectByExample(example);
        for (int i = comments.size() - 1; i >= 0; i--) {
            FkComment comment=comments.get(i);
            FkMiniUser miniUser = miniUserMapper.select(new FkMiniUser(comment.getSenderId())).get(0);
            if (StringUtil.isNotEmpty(miniUser.getRemarks()) && "black".equals(miniUser.getRemarks())) {
                comments.remove(comment);
            }
        }
        /*Iterator<FkComment> commentIterator = comments.iterator();

        while (commentIterator.hasNext()) {
            FkComment comment = commentIterator.next();

        }
        comments = Lists.newArrayList(commentIterator);*/
        return comments;
    }
}

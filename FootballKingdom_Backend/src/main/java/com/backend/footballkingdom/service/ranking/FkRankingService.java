package com.backend.footballkingdom.service.ranking;

import com.backend.footballkingdom.mapper.FkRankingMapper;
import com.backend.footballkingdom.model.ranking.FkRanking;
import com.backend.footballkingdom.model.ranking.vo.RankingPageRequest;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkRankingService {
    @Resource
    private FkRankingMapper fkRankingMapper;

    @Transactional
    public int addRanking(FkRanking fkRanking) {
        fkRanking.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkRankingMapper.insertSelective(fkRanking);
    }

    @Transactional
    public int deleteRankingByPrimaryKey(FkRanking fkRanking) {
        return fkRankingMapper.deleteByPrimaryKey(fkRanking);
    }

    @Transactional
    public int updateRankingByPrimaryKey(FkRanking fkRanking) {
        return fkRankingMapper.updateByPrimaryKeySelective(fkRanking);
    }

    public List<FkRanking> getAllRanking() {
        return fkRankingMapper.selectAll();
    }

    public List<FkRanking> getRankingByTemplate(FkRanking fkRanking) {
        return fkRankingMapper.select(fkRanking);
    }

    public List<FkRanking> getWorldCupRanking(RankingPageRequest fkRanking){
        return fkRankingMapper.selectRankingAllInfo(fkRanking);
    }

    public List<FkRanking> getAllKindsByLeague(RankingPageRequest fkRanking) {
        Example example = new Example(FkRanking.class);
        Example.Criteria criteria = example.createCriteria();
        example.setDistinct(true);
        example.selectProperties("kind");
        criteria.andEqualTo("leagueId",fkRanking.getLeagueId());
        return fkRankingMapper.selectByExample(example);
    }

    public Page<FkRanking> getRankingByExampleAndPage(RankingPageRequest rankingPageRequest) {
        PageHelper.startPage(rankingPageRequest.getRankingPageNum(), rankingPageRequest.getRankingPageSize());
        Page<FkRanking> fkRankings=( Page<FkRanking>)fkRankingMapper.selectRankingAllInfo(rankingPageRequest);
        return fkRankings;
    }

}

package com.backend.footballkingdom.service.teamList.impl;

import com.backend.footballkingdom.controller.teamList.vo.TeamListRequest;
import com.backend.footballkingdom.mapper.FkTeamListMapper;
import com.backend.footballkingdom.model.teamList.FkTeamList;
import com.backend.footballkingdom.service.teamList.FkTeamListService;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FkTeamListServiceImpl implements FkTeamListService{
    @Resource
    private FkTeamListMapper fkTeamListMapper;

    @Override
    public Page<FkTeamList> findByPage(TeamListRequest request) {
        PageHelper.startPage(request.getPageNum(), request.getPageSize());
        List<FkTeamList> list = new ArrayList<>();
        list = fkTeamListMapper.findAll(request);
        Page<FkTeamList> listPage = (Page<FkTeamList>) list;
        return listPage;
    }

    @Override
    @Transactional
    public int addOne(FkTeamList teamList) {
        teamList.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkTeamListMapper.insertSelective(teamList);
    }

    @Override
    @Transactional
    public int updateOne(FkTeamList teamList) {
        return fkTeamListMapper.updateByPrimaryKeySelective(teamList);
    }

    @Override
    public int deleteOne(FkTeamList teamList) {
        return fkTeamListMapper.deleteByPrimaryKey(teamList);
    }

    /*@Transactional
    public int addTeamList(FkTeamList fkTeamList) {
        return fkTeamListMapper.insertSelective(fkTeamList);
    }

    @Transactional
    public int deleteTeamListByPrimaryKey(FkTeamList fkTeamList) {
        return fkTeamListMapper.deleteByPrimaryKey(fkTeamList);
    }

    @Transactional
    public int updateTeamListByPrimaryKey(FkTeamList fkTeamList) {
        return fkTeamListMapper.updateByPrimaryKeySelective(fkTeamList);
    }

    public List<FkTeamList> getAllTeamList() {
        return fkTeamListMapper.selectAll();
    }

    public List<FkTeamList> getTeamListByTemplate(FkTeamList fkTeamList) {
        return fkTeamListMapper.select(fkTeamList);
    }

    public FkTeamList getOneTeamListByPrimaryKey(FkTeamList fkTeamList) {
        return fkTeamListMapper.selectByPrimaryKey(fkTeamList);
    }*/

}

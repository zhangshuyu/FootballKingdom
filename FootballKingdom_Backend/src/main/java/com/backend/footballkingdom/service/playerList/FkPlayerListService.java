package com.backend.footballkingdom.service.playerList;

import com.backend.footballkingdom.controller.player.vo.PlayerListRequest;
import com.backend.footballkingdom.mapper.FkPlayerListMapper;
import com.backend.footballkingdom.model.playerList.FkPlayerList;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FkPlayerListService {
    @Resource
    private FkPlayerListMapper fkPlayerListMapper;

    public Page<FkPlayerList> findByPage(PlayerListRequest request) {
        PageHelper.startPage(request.getPageNum(), request.getPageSize());
        List<FkPlayerList> list = new ArrayList<>();
        list = fkPlayerListMapper.findAll(request);
        Page<FkPlayerList> listPage = (Page<FkPlayerList>) list;
        return listPage;
    }

    @Transactional
    public int addPlayerList(FkPlayerList fkPlayerList) {
        fkPlayerList.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkPlayerListMapper.insertSelective(fkPlayerList);
    }

    @Transactional
    public int deletePlayerListByPrimaryKey(FkPlayerList fkPlayerList) {
        return fkPlayerListMapper.deleteByPrimaryKey(fkPlayerList);
    }

    @Transactional
    public int updatePlayerListByPrimaryKey(FkPlayerList fkPlayerList) {
        return fkPlayerListMapper.updateByPrimaryKey(fkPlayerList);
    }

    public List<FkPlayerList> getAllPlayerList() {
        return fkPlayerListMapper.selectAll();
    }

    public List<FkPlayerList> getPlayerListByTemplate(FkPlayerList fkPlayerList) {
        return fkPlayerListMapper.select(fkPlayerList);
    }

    public FkPlayerList getOnePlayerListByPrimaryKey(FkPlayerList fkPlayerList) {
        return fkPlayerListMapper.selectOne(fkPlayerList);
    }
}

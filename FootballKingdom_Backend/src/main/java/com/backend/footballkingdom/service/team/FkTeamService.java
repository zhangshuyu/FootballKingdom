package com.backend.footballkingdom.service.team;

import com.backend.footballkingdom.mapper.FkTeamMapper;
import com.backend.footballkingdom.model.team.FkTeam;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkTeamService {
    @Resource
    private FkTeamMapper fkTeamMapper;

    @Transactional
    public int addTeam(FkTeam fkTeam) {
        fkTeam.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkTeamMapper.insertSelective(fkTeam);
    }

    @Transactional
    public int deleteTeamByPrimaryKey(FkTeam fkTeam) {
        fkTeam.setDelFlag("1");
        return fkTeamMapper.updateByPrimaryKeySelective(fkTeam);
    }

    @Transactional
    public int updateTeamByPrimaryKey(FkTeam fkTeam) {
        return fkTeamMapper.updateByPrimaryKeySelective(fkTeam);
    }

    public List<FkTeam> getAllTeam() {
        return fkTeamMapper.selectAll();
    }

    public List<FkTeam> getAllTeamNotDelete() {
        Example example = new Example(FkTeam.class);
        example.createCriteria().andEqualTo("delFlag","0");
        return fkTeamMapper.selectByExample(example);
    }

    public Page<FkTeam> getAllTeamByPage(int pageNum, int pageSiz) {
        PageHelper.startPage(pageNum, pageSiz);
        Example example = new Example(FkTeam.class);
        example.createCriteria().andEqualTo("delFlag","0");
        Page<FkTeam> teamPage = (Page<FkTeam>) fkTeamMapper.selectByExample(example);
        return teamPage;
    }

    public List<FkTeam> getTeamByTemplate(FkTeam fkTeam) {
        return fkTeamMapper.select(fkTeam);
    }

    public FkTeam getOneTeamByPrimaryKey(String fkTeamID) {
        FkTeam fkTeam=new FkTeam();
        fkTeam.setId(fkTeamID);
        return fkTeamMapper.selectOne(fkTeam);
    }
}

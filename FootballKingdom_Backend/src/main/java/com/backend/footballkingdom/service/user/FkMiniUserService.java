package com.backend.footballkingdom.service.user;

import com.backend.footballkingdom.controller.user.vo.MiniUserRequest;
import com.backend.footballkingdom.mapper.FkMiniUserMapper;
import com.backend.footballkingdom.model.ranking.FkRanking;
import com.backend.footballkingdom.model.user.FkMiniUser;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkMiniUserService {

    @Resource
    private FkMiniUserMapper miniUserMapper;

    @Transactional
    public String addMiniUser(FkMiniUser fkMiniUser) {
        String uuid = UUIDUtils.randomUUIDWithoutLine();
        fkMiniUser.setId(uuid);
        Assert.isTrue(miniUserMapper.insertSelective(fkMiniUser) == 1, "添加用户openid失败：");
        return uuid;
    }

    @Transactional
    public int updateMiniUser(FkMiniUser fkMiniUser) {
        return miniUserMapper.updateByPrimaryKeySelective(fkMiniUser);
    }

    public List<FkMiniUser> getMiniUserByExample(MiniUserRequest fkMiniUser) {
        Example example = new Example(FkMiniUser.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtil.isNotEmpty(fkMiniUser.getOpenid())) {
            criteria.andEqualTo("openid", fkMiniUser.getOpenid());
        }
        if (fkMiniUser.getCreateDate()!=null) {
            criteria.andEqualTo("createDate", fkMiniUser.getCreateDate());
        }
        example.orderBy("createDate").desc();
        return miniUserMapper.selectByExample(example);
    }



    public Page<FkMiniUser> getMiniUserByPage(MiniUserRequest fkMiniUser) {
        PageHelper.startPage(fkMiniUser.getPageNum(), fkMiniUser.getPageSize());
        Example example = new Example(FkMiniUser.class);
        Example.Criteria criteria=example.createCriteria();
        if (StringUtil.isNotEmpty(fkMiniUser.getNickName())){
            criteria.andEqualTo("nickName",fkMiniUser.getNickName());
        }
        example.orderBy("createDate").desc();
        Page<FkMiniUser> miniUsers=(Page<FkMiniUser>)miniUserMapper.selectByExample(example);
        return miniUsers;
    }

}

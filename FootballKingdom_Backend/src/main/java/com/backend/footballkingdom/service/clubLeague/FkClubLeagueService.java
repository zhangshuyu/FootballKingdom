package com.backend.footballkingdom.service.clubLeague;

import com.backend.footballkingdom.mapper.FkClubLeagueMapper;
import com.backend.footballkingdom.model.clubLeague.FkClubLeague;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkClubLeagueService {
    @Resource
    private FkClubLeagueMapper fkClubLeagueMapper;

    @Transactional
    public int addClubLeague(FkClubLeague fkClubLeague) {
        return fkClubLeagueMapper.insertSelective(fkClubLeague);
    }

    @Transactional
    public int deleteClubLeagueByPrimaryKey(FkClubLeague fkClubLeague) {
        return fkClubLeagueMapper.deleteByPrimaryKey(fkClubLeague);
    }

    @Transactional
    public int updateClubLeagueByPrimaryKey(FkClubLeague fkClubLeague) {
        return fkClubLeagueMapper.updateByPrimaryKeySelective(fkClubLeague);
    }

    public List<FkClubLeague> getAllClubLeague() {
        return fkClubLeagueMapper.selectAll();
    }

    public List<FkClubLeague> getClubLeagueByTemplate(FkClubLeague fkClubLeague) {
        return fkClubLeagueMapper.select(fkClubLeague);
    }

    public FkClubLeague getOneClubLeagueByPrimaryKey(FkClubLeague fkClubLeague) {
        return fkClubLeagueMapper.selectOne(fkClubLeague);
    }
}

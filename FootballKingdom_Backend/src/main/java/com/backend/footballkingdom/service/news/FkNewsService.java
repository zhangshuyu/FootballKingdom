package com.backend.footballkingdom.service.news;

import com.backend.footballkingdom.model.news.FkNews;
import com.backend.footballkingdom.model.news.vo.NewsPageRequest;
import com.github.pagehelper.Page;

import java.util.List;

public interface FkNewsService {

    public int addNews(FkNews news);

    int countImportantWorldCupNews();

    int updateNews(FkNews news);

    Page<FkNews> getNewsByPage(NewsPageRequest page);

    int removeWithDelFag(FkNews news);

    Page<FkNews> getNewsByExamle(NewsPageRequest fkNews);

    List<FkNews> findWorldCupNews();

    FkNews findById(String id);

}

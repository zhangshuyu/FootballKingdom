package com.backend.footballkingdom.service.user;

import com.backend.footballkingdom.mapper.FkAdminUserMapper;
import com.backend.footballkingdom.model.user.FkAdminUser;
import com.backend.footballkingdom.util.UUIDUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkAdminUserService {

    @Resource
    private FkAdminUserMapper adminUserMapper;

    public List<FkAdminUser> getAllAdminUser(){
        return adminUserMapper.selectAll();
    }

    public List<FkAdminUser> getAdminUserByTemplate(FkAdminUser adminUser) {
        return adminUserMapper.select(adminUser);
    }
}

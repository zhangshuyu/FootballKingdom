package com.backend.footballkingdom.service.live;

import com.backend.footballkingdom.mapper.FkLiveMapper;
import com.backend.footballkingdom.model.live.FkLive;
import com.backend.footballkingdom.model.live.vo.LivePageRequest;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkLiveService {

    @Resource
    private FkLiveMapper fkLiveMapper;

    @Transactional
    public int addLive(FkLive fkLive) {
        fkLive.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkLiveMapper.insertSelective(fkLive);
    }

    @Transactional
    public int deleteLiveByPrimaryKey(FkLive fkLive) {
        return fkLiveMapper.deleteByPrimaryKey(fkLive);
    }

    @Transactional
    public int updateLiveByPrimaryKey(FkLive fkLive) {
        return fkLiveMapper.updateByPrimaryKeySelective(fkLive);
    }

    public List<FkLive> getAllLive() {
        return fkLiveMapper.selectAll();
    }

    public List<FkLive> getLiveByTemplate(FkLive fkLive) {
        return fkLiveMapper.select(fkLive);
    }

    public FkLive getOneLivePrimaryKey(String id) {
        FkLive fkLive=new FkLive();
        fkLive.setId(id);
        return fkLiveMapper.selectOne(fkLive);
    }

    public List<FkLive> getAllLiveNotDelete() {
        Example example = new Example(FkLive.class);
        example.createCriteria().andEqualTo("delFlag","0");
        return fkLiveMapper.selectByExample(example);
    }

    public int countImportantLive() {
        Example example = new Example(FkLive.class);
        example.createCriteria().andEqualTo("importantLiveFlag",'1').andEqualTo("delFlag","0");
        int i=fkLiveMapper.selectCountByExample(example);
        return i;
    }

    public int countImportantWorldCupLive() {
        Example example = new Example(FkLive.class);
        example.createCriteria().andEqualTo("importantWorldCupFlag",'1').andEqualTo("delFlag","0");
        return fkLiveMapper.selectCountByExample(example);
    }

    public int countRecommendedLive() {
        Example example = new Example(FkLive.class);
        example.createCriteria().andEqualTo("recommendedLiveFlag",'1');
        return fkLiveMapper.selectCountByExample(example);
    }

    public Page<FkLive> getLiveByExampleAndPage(LivePageRequest livePageRequest) {
        PageHelper.startPage(livePageRequest.getLivePageNum(), livePageRequest.getLivePageSize());
        Page<FkLive> fkLivePage=( Page<FkLive>)fkLiveMapper.selectLiveAllInfo(livePageRequest);
        return fkLivePage;
    }

    public List<FkLive> getLiveByExample(FkLive fkLive) {
        Example example = new Example(FkLive.class);
        Example.Criteria criteria=example.createCriteria();
        if (StringUtil.isNotEmpty(fkLive.getDelFlag())){
            criteria.andEqualTo("delFlag",fkLive.getDelFlag());
        }
        if (StringUtil.isNotEmpty(fkLive.getImportantLiveFlag())){
            criteria.andEqualTo("importantLiveFlag",fkLive.getImportantLiveFlag());
        }
        if (StringUtil.isNotEmpty(fkLive.getImportantWorldCupFlag())){
            criteria.andEqualTo("importantWorldCupFlag",fkLive.getImportantWorldCupFlag());
        }
        return fkLiveMapper.selectByExample(example);
    }

}

package com.backend.footballkingdom.service.league;

import com.backend.footballkingdom.mapper.FkLeagueMapper;
import com.backend.footballkingdom.model.league.FkLeague;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkLeagueService {

    @Resource
    private FkLeagueMapper leagueMapper;
    

    public List<FkLeague> getAllLeagueNotDelete() {
        Example example = new Example(FkLeague.class);
        example.createCriteria().andEqualTo("delFlag","0");
        return leagueMapper.selectByExample(example);
    }

    public Page<FkLeague> getAllLeagueByPage(int pageNum,int pageSiz) {
        Example example = new Example(FkLeague.class);
        example.orderBy("delFlag").asc();
        PageHelper.startPage(pageNum, pageSiz);
        Page<FkLeague> leaguePage=(Page<FkLeague>)leagueMapper.selectByExample(example);
        return leaguePage;
    }

    public List<FkLeague> getLeagueByTemplate(FkLeague fkLeague) {
        return leagueMapper.select(fkLeague);
    }

    @Transactional
    public int addLeague(FkLeague fkLeague){
        fkLeague.setId(UUIDUtils.randomUUIDWithoutLine());
        return leagueMapper.insertSelective(fkLeague);
    }

    @Transactional
    public int updateLeagueByPrimaryKey(FkLeague fkLeague){
        return leagueMapper.updateByPrimaryKeySelective(fkLeague);
    }

    @Transactional
    public int deleteLeagueByPrimaryKey(FkLeague fkLeague){
        return leagueMapper.deleteByPrimaryKey(fkLeague);
    }
}

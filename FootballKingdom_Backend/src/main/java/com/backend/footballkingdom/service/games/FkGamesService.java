package com.backend.footballkingdom.service.games;

import com.backend.footballkingdom.mapper.FkGamesMapper;
import com.backend.footballkingdom.model.games.FkGames;
import com.backend.footballkingdom.model.games.GamesPageRequest;
import com.backend.footballkingdom.util.UUIDUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkGamesService {
    @Resource
    private FkGamesMapper fkGamesMapper;

    @Transactional
    public int addGames(FkGames fkGames) {
        fkGames.setId(UUIDUtils.randomUUIDWithoutLine());
        return fkGamesMapper.insertSelective(fkGames);
    }

    @Transactional
    public int deleteGamesByPrimaryKey(FkGames fkGames) {
        return fkGamesMapper.deleteByPrimaryKey(fkGames);
    }

    @Transactional
    public int updateGamesByPrimaryKey(FkGames fkGames) {
        return fkGamesMapper.updateByPrimaryKeySelective(fkGames);
    }

    public List<FkGames> getAllGames() {
        return fkGamesMapper.selectAll();
    }

    public List<FkGames> getGamesByTemplate(FkGames fkGames) {
        return fkGamesMapper.select(fkGames);
    }

    public List<FkGames> findGamesByStartDayWithLeagueAndTeam(GamesPageRequest gamesPageRequest) {
        PageHelper.startPage(gamesPageRequest.getPageNum(),gamesPageRequest.getPageSize());
        return fkGamesMapper.findGamesByStartDayWithLeagueAndTeam(gamesPageRequest);
    }

    public List<FkGames> findGameByIdWithLeagueAndTeam(GamesPageRequest gamesPageRequest) {
        return fkGamesMapper.findGamesByStartDayWithLeagueAndTeam(gamesPageRequest);
    }

    public FkGames getOneGamesByPrimaryKey(FkGames fkGames) {
        return fkGamesMapper.selectOne(fkGames);
    }

    public List<FkGames> findGamesWithLeagueAndTeam(GamesPageRequest gamesPageRequest) {
        PageHelper.startPage(gamesPageRequest.getPageNum(),gamesPageRequest.getPageSize());
        return fkGamesMapper.findGamesWithLeagueAndTeam(gamesPageRequest);
    }

}

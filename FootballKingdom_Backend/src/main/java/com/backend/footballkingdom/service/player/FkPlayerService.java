package com.backend.footballkingdom.service.player;

import com.backend.footballkingdom.mapper.FkPlayerMapper;
import com.backend.footballkingdom.model.player.FkPlayer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FkPlayerService {
    @Resource
    private FkPlayerMapper fkPlayerMapper;

    @Transactional
    public int addPlayer(FkPlayer fkPlayer) {
        return fkPlayerMapper.insertSelective(fkPlayer);
    }

    @Transactional
    public int deletePlayerByPrimaryKey(FkPlayer fkPlayer) {
        return fkPlayerMapper.deleteByPrimaryKey(fkPlayer);
    }

    @Transactional
    public int updatePlayerByPrimaryKey(FkPlayer fkPlayer) {
        return fkPlayerMapper.updateByPrimaryKeySelective(fkPlayer);
    }

    public List<FkPlayer> getAllPlayer() {
        return fkPlayerMapper.selectAll();
    }

    public List<FkPlayer> getPlayerByTemplate(FkPlayer fkPlayer) {
        return fkPlayerMapper.select(fkPlayer);
    }

    public FkPlayer getOnePlayerByPrimaryKey(FkPlayer fkPlayer) {
        return fkPlayerMapper.selectOne(fkPlayer);
    }
}

package com.backend.footballkingdom.enums;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public enum ZBBRequestMethod {

    GET_DATA_URL("GET_DATA_URL","获取数据","https://dc.qiumibao.com/shuju/public/index.php?_url=/data/index&league=LEAGUE&type=TYPE&tab=TAB&year=[year]",HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8);

    private String code;

    private String desc;

    private String url;

    private HttpMethod method;

    private MediaType mediaType;

    ZBBRequestMethod(String c, String d, String u, HttpMethod m, MediaType mt) {
        this.code = c;
        this.desc = d;
        this.url = u;
        this.method = m;
        this.mediaType = mt;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getUrl() {
        return url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public MediaType getMediaType() {
        return mediaType;
    }
}

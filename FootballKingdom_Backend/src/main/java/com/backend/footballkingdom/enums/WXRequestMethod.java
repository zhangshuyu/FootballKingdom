package com.backend.footballkingdom.enums;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public enum WXRequestMethod {

    /**
     * 公众号接口
     */
    ACCESS_TOKEN_URL("ACCESS_TOKEN_URL", "获取accessToken", "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET", HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8),
    GET_USERINFO_URL("GET_USERINFO_URL","获取用户信息","https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN",HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8),
    CREATE_MENU_URL("CREATE_MENU_URL", "创建自定义菜单", "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN", HttpMethod.POST, MediaType.APPLICATION_JSON_UTF8),
    WEB_AUTHORIZATION_URL("WEB_AUTHORIZATION_URL", "网页授权", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect", HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8),
    GET_AUTHORIZATION_URL("GET_AUTHORIZATION_URL","获取openid","https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=APPSECRET&code=CODE&grant_type=authorization_code",HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8),

    /**
     * 小程序接口
     */
    MINI_GET_AUTHORIZATION_URL("MINI_GET_AUTHORIZATION_URL","获取小程序openid","https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code",HttpMethod.GET, MediaType.APPLICATION_JSON_UTF8);

    private String code;

    private String desc;

    private String url;

    private HttpMethod method;

    private MediaType mediaType;

    WXRequestMethod(String c, String d, String u, HttpMethod m, MediaType mt) {
        this.code = c;
        this.desc = d;
        this.url = u;
        this.method = m;
        this.mediaType = mt;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getUrl() {
        return url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public MediaType getMediaType() {
        return mediaType;
    }
}

package com.backend.footballkingdom.security;

import com.backend.footballkingdom.model.user.FkAdminUser;
import com.backend.footballkingdom.service.user.FkAdminUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component(value = "shiroService")
public class ShiroServiceImpl implements ShiroService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FkAdminUserService adminUserService;

    @Override
    public boolean userExist(String userName, String password) {
        FkAdminUser adminUserTemplate=new FkAdminUser();
        adminUserTemplate.setEmail(userName);
        adminUserTemplate.setPassword(password);

        List<FkAdminUser> result_admin_users=adminUserService.getAdminUserByTemplate(adminUserTemplate);

        if (1!=result_admin_users.size()){
            return false;
        }
        return password.equals(result_admin_users.get(0).getPassword());
    }

    @Override
    public boolean isPassed(String userName, String password) {
        return false;
    }

    @Override
    public Set<String> getUserRoleNames(String userName) {
        FkAdminUser adminUserTemplate=new FkAdminUser();
        adminUserTemplate.setEmail(userName);
        List<FkAdminUser> result_admin_users=adminUserService.getAdminUserByTemplate(adminUserTemplate);
        Set<String> set = new HashSet<String>();
        set.add(result_admin_users.get(0).getUserRole());
        return set;
    }

    @Override
    public Set<String> getUserPermission(String userName) {
        Set<String> set = new HashSet<String>();
        set.add("/fk/admin/*");
        set.add("/fk/admin/**/*");
        return set;
    }

    @Override
    public Map<String, String> getDefinitionMap() {
        return null;
    }
}

$(".comment").hide();
$("title").html(live.liveStatus === "直播中" ? "正在直播中" : "足球王国");

sessionStorage.setItem('remarks', 'early');

//var baseUrl = "http://127.0.0.1:8090/api/";
var baseUrl = "https://www.zqwg.top/api/";

var toast = new auiToast();
var dialog = new auiDialog({})
var searchBarInput = document.querySelector(".aui-searchbar input");

var player = videojs('live-stream', {}, function () {
    console.log('Good to go!');
});
player.on('play', function () {
    console.log('开始/恢复播放');
    begin_playing();
});
player.on('pause', function () {
    console.log('暂停播放');
});
player.on('ended', function () {
    console.log('结束播放');
});

player.on(['loadstart', 'play', 'playing', 'firstplay', 'pause', 'ended', 'adplay', 'adplaying', 'adfirstplay', 'adpause', 'adended', 'contentplay', 'contentplaying', 'contentfirstplay', 'contentpause', 'contentended', 'contentupdate'], function (e) {
    console.warn('VIDEOJS player event: ', e.type);
});

var tab = new auiTab({
    element: document.getElementById("tab"),
    index: 1,
    repeatClick: false
}, function (ret) {
    if (ret.index === 1) {
        $("#introduction").show();
        $(".comment").hide();
    }
    if (ret.index === 2) {
        $("#introduction").hide();
        $(".comment").show();
    }
});


/*$('#comment').on('scroll',function(){
    var top=$('#comment').scrollTop()
    if ( top<= 2) {
        var pullRefresh = new auiPullToRefresh({
            container: document.querySelector('.aui-refresh-content'),
            triggerDistance: 100
        }, function (ret) {
            if (ret.status == "success") {
                toast.loading(
                    {
                        title: "加载评论中",
                        duration: 1100
                    },
                    function (ret) {
                        if (ret.status == "success") {
                            setTimeout(function () {
                                toast.hide();
                                pullRefresh.cancelLoading();
                                var req = {
                                    liveId: live.id,
                                    remarks: sessionStorage.getItem('remarks'),
                                };
                                ajaxForComment(req);
                            }, 1100)
                        }
                    });
            }
        })
    }
    if ($('#comment').scrollTop() > 2) {
        pullRefresh.remove()
    }
});*/

$(".comment-headx").click(function () {
    toast.loading(
        {
            title: "加载评论中",
            duration: 1100,
        },
        function (ret) {
            if (ret.status == "success") {
                setTimeout(function () {
                    toast.hide();
                    var req = {
                        liveId: live.id,
                        remarks: sessionStorage.getItem('remarks'),
                    };
                    ajaxForComment(req);
                }, 1100)
            }
        });
});


/*var pullRefresh = new auiPullToRefresh({
    container: document.querySelector('.aui-refresh-content'),
    triggerDistance: 100
}, function (ret) {
    if (ret.status == "success") {
        toast.loading(
            {
                title: "加载评论中",
                duration: 1100,
            },
            function (ret) {
                if (ret.status == "success") {
                    setTimeout(function () {
                        toast.hide();
                        pullRefresh.cancelLoading();
                        var req = {
                            liveId: live.id,
                            remarks: sessionStorage.getItem('remarks'),
                        };
                        ajaxForComment(req);
                    }, 1100)
                }
            });
    }
})*/


$('input[type=radio][name=radio]').change(function () {
    console.log(this.value);
    sessionStorage.setItem('remarks', this.value);
    var req = {
        liveId: live.id,
        remarks: this.value
    };
    ajaxForComment(req);
});

var inputsearch = new auisearch(function (ret) {
    doSearch(ret);
});

function ajaxForComment(req) {
    $('#allComments').load(baseUrl + "fk/watchLive/getComments", req);
};

function begin_playing() {
    $('.video-head').fadeOut()
};

function doSearch(comment_content) {
    console.log(comment_content);
    $.ajax({
        cache: false,
        async: false,
        url: baseUrl + "fk/watchLive/addComment",
        timeout: 15000,
        data: {
            senderId: openid,
            liveId: live.id,
            content: comment_content,
        },
        dataType: 'json',
        success: function (data) {
            var res = eval(data);
            if (res.status == 200) {
                toast.success(
                    {
                        title: "评论成功",
                        duration: 2000
                    },
                    function (ret) {
                        console.log(ret);
                        setTimeout(function () {
                            toast.hide();
                        }, 2000)
                    });
                var req = {
                    liveId: live.id,
                    remarks: sessionStorage.getItem('remarks'),
                };

                ajaxForComment(req);
            } else {
                dialog.alert({
                    title: "评论失败！",
                    msg: res.message,
                    buttons: ['确定']
                }, function (ret) {
                    console.log(ret)
                })
            }
            searchBarInput.value = '';
            searchBarInput.blur();
        }
    });
}

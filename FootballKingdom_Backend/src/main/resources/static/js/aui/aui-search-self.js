var auisearch = function (callback){
    var searchBar = document.querySelector(".aui-searchbar");
    var search = document.querySelector("#search");
    var mask = document.querySelector("#mask");
    var searchBarInput = document.querySelector(".aui-searchbar input");
    var searchBarBtn = document.querySelector(".aui-searchbar .aui-searchbar-btn");
    var searchBarClearBtn = document.querySelector(".aui-searchbar .aui-searchbar-clear-btn");
    if(searchBar){
        searchBarInput.onclick = function(){
            searchBarBtn.style.marginRight = 0;
            search.classList.remove("comment-input-bottom");
            search.classList.add("comment-input-top");
            mask.classList.add("aui-mask-in");
        }
        searchBarInput.oninput = function(){
            if(this.value.length){
                searchBarClearBtn.style.display = 'block';
                searchBarBtn.classList.add("aui-text-info");
                searchBarBtn.textContent = "提交";
            }else{
                searchBarClearBtn.style.display = 'none';
                searchBarBtn.classList.remove("aui-text-info");
                searchBarBtn.textContent = "取消";
            }
        }
    }
    searchBarClearBtn.onclick = function(){
        this.style.display = 'none';
        searchBarInput.value = '';
        searchBarBtn.classList.remove("aui-text-info");
        searchBarBtn.textContent = "取消";
        search.classList.remove("comment-input-top");
        search.classList.add("comment-input-bottom");
        mask.classList.remove("aui-mask-in");
    };
    searchBarBtn.onclick = function(){
        var keywords = searchBarInput.value;
        if(keywords.length){
            searchBarInput.blur();
            callback(keywords.trim());
            //document.getElementById("search-keywords").textContent = keywords;
        }else{
            this.style.marginRight = "-"+this.offsetWidth+"px";
            searchBarInput.value = '';
            searchBarInput.blur();
        }
        search.classList.remove("comment-input-top");
        search.classList.add("comment-input-bottom");
        mask.classList.remove("aui-mask-in");
    }
}